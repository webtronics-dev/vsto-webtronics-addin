﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Office.Tools.Ribbon;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;
using WebtronicsAddIn.Tools.OnRibbon.About;
using System.Net.Http;

namespace WebtronicsAddIn
{
    public partial class webtronicsRibbon
    {
        public static readonly HttpClient httpClient =  new HttpClient();
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {
        }

        private void contentsCreateRibbon_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            Globals.ThisAddIn.contentsCreatorCallForm();
        }
        private void UrlToHyperlinkRibbonButton_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            Globals.ThisAddIn.UrlToHyperlinkCallForm();
        }

        private void filterToNewSheetRibbonButton_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            Globals.ThisAddIn.FilterToNewSheetCallForm();
        }

        private void MergeBooksRibbonButton_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            Globals.ThisAddIn.BooksMergeCallForm();
        }

        private void KeyCollectorRibbonSummFrequency_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            Globals.ThisAddIn.KeyCollectorSumFrequencyCallForm();
        }

        private void TransormSheetForGoogleSheets_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            Globals.ThisAddIn.KeyCollectorTransformForGSheetsCallForm();
        }

        private void SortColumnsOnAllSheetsButton_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            Globals.ThisAddIn.SortColumnsOnSelectedSheetsCallForm();
        }     

        private void ColumnsWidthChangerButton_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            var form = new Tools.OnRibbon.Misc.ColumnsWidthChanger.ColumnsWidthChangerForm();
            form.ShowDialog();
        }

        private void AboutButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new Tools.OnRibbon.About.AboutForm(httpClient);
            form.ShowDialog();
        }

        private void WordsHighligherRibbonButton_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            var form = new Tools.OnRibbon.Misc.WordsHighlighter.WordsHighlighterForm();
            form.ShowDialog();
        }

        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            var form = new Tools.OnRibbon.KeyCollector.CreateFileForTopVisor.CreateFileForTopVisorForm();
            form.ShowDialog();
        }

        private void Button_KC_CreateMenu_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            var form = new Tools.OnRibbon.KeyCollector.CreateMenuFile.CreateMenuFileForm();
            form.ShowDialog();
        }

        private void XMLParserRibbonButton_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            var form = new Tools.OnRibbon.Misc.XmlParser.XmlParserForm();
            form.ShowDialog();
        }

        private void RemoveColumnsWithoutData_RibbonButton_Click(object sender, RibbonControlEventArgs e)
        {
            if (!AddinLicense.IsLicenseNotActive(httpClient)) return;
            if (ExcelFunctions.IsNoBooksOpenedWithMessage()) return;
            var form = new Tools.OnRibbon.Misc.RemoveColumnsWithoutData.RemoveColumnsWithoutDataForm();
            form.ShowDialog();
        }
    }
}
