﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using WebtronicsAddIn.Tools.Functions;
using WebtronicsAddIn.Tools.OnRibbon.Contents;
using WebtronicsAddIn.Tools.OnRibbon.Misc.UrlToHyperlink;


namespace WebtronicsAddIn
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        public void  contentsCreatorCallForm()
        {
            var sheets = ExcelFunctions.GetAllSheetsOfActiveWorkbook();
            var dictionarySheets = ExcelFunctions.CreateDictionaryOfSheets(sheets);


            var mainContents = new ContentsCreatorForm(dictionarySheets);

            mainContents.ShowDialog();

        }

        public void UrlToHyperlinkCallForm()
        {
            var dictionarySheets = ExcelFunctions.CreateDictionaryOfSheets(ExcelFunctions.GetAllSheetsOfActiveWorkbook());

            UrlToHyperlinkForm form = new UrlToHyperlinkForm(dictionarySheets);
            form.ShowDialog();

        }

        public void FilterToNewSheetCallForm()
        {
            var form = new WebtronicsAddIn.Tools.OnRibbon.Misc.FilterToNewSheet.FilterToNewSheetForm();
            form.ShowDialog();
        }
        
        public void BooksMergeCallForm()
        {
            var form = new WebtronicsAddIn.Tools.OnRibbon.Misc.BooksMerge.BooksMergeForm();
            form.ShowDialog();
        }

        public void KeyCollectorSumFrequencyCallForm()
        {
            var form = new WebtronicsAddIn.Tools.OnRibbon.KeyCollector.SummFrequency.KeyCollectorSummFrequencyForm();
            form.ShowDialog();
        }

        public void KeyCollectorTransformForGSheetsCallForm()
        {
            var form = new WebtronicsAddIn.Tools.OnRibbon.KeyCollector.TransformSheetForGSheets.TransformSheetForGSheetsForm();
            form.ShowDialog();
        }

        public void SortColumnsOnSelectedSheetsCallForm()
        {
            var form = new Tools.OnRibbon.Misc.SortColumnsOnSelectedSheets.SortColumnsOnSelectedSheetsForm();
            form.ShowDialog();
        }

        #region Код, автоматически созданный VSTO

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
