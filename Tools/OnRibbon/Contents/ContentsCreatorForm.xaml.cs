﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.OnRibbon.ContentsNS;

namespace WebtronicsAddIn.Tools.OnRibbon.Contents
{
    /// <summary>
    /// Логика взаимодействия для ContentsCreatorForm.xaml
    /// </summary>
    public partial class ContentsCreatorForm : Window
    {
        public List<ContentsCreator.SheetInList> sheets { get; set; }


        public ContentsCreatorForm(Dictionary<int, string> sheetsDic)
        {
            InitializeComponent();
            AddSheetsDictionaryToListView(sheetsDic);
        }

        private void cellFromTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsGood);
        }

        private void cellFromTextBox_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsGood))
                e.CancelCommand();
        }

        bool IsGood(char c)
        {
            if (c >= '0' && c <= '9')
                return true;
            if (c >= 'a' && c <= 'f')
                return true;
            if (c >= 'A' && c <= 'F')
                return true;
            return false;
        }

        private void AddSheetsDictionaryToListView(Dictionary<int,string> sheetsDic)
        {
            sheets = new List<ContentsCreator.SheetInList>();

            foreach (KeyValuePair<int,string> sheet in sheetsDic)
            {
                sheets.Add(new ContentsCreator.SheetInList() { CheckedSheet = sheet.Value == "Оглавление" ? false : true, SheetNumber = sheet.Key, SheetName = sheet.Value }); ;
            }

            myGrid.ItemsSource = sheets;
        }

        private void ButtonCreateContents_Click(object sender, RoutedEventArgs e)
        {
            var settings = GetSettings();
            var contents = new ContentsCreator(settings);
            contents.startCreateContents();

            MessageBox.Show("Готово!","Оглавление создано!");
            Close();
        }

        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var sheetList in sheets)
            {
                sheetList.CheckedSheet = true;
            }
            myGrid.ItemsSource = null;
            myGrid.ItemsSource = sheets;
        }

        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var sheetList in sheets)
            {
                sheetList.CheckedSheet = false;
            }

            myGrid.ItemsSource = null;
            myGrid.ItemsSource = sheets;
        }

        public ContentsCreator.Settings GetSettings()
        {
            
            var checkedSheets = from sh in sheets
                                where sh.CheckedSheet == true
                                select sh;

            var settings = new ContentsCreator.Settings
            {
                isNameFromSheet = (bool)nameFromSheet.IsChecked,
                cellRangeForContentsName = cellRangeForContentsName.Text,
                isCreateLinkToContentsOnAllSheets = (bool)isCreateLinkToContentsOnAllSheets.IsChecked,
                isCleanContentsSheet = (bool)isCleanContentsSheet.IsChecked,
                isCountQuantityOfRows = (bool)isCountQuantityOfRows.IsChecked,
                sheetsToContents = checkedSheets.ToList()
            };

            return settings;

        }

        private void CloseButt_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
