﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using WebtronicsAddIn.Tools.Functions;

namespace WebtronicsAddIn.Tools.OnRibbon.ContentsNS
{
   public class ContentsCreator
    {
        public ContentsCreator(Settings _settings)
        {
            settings = _settings;
        }

        public Settings settings { get; set; }
        public Excel.Worksheet contentSheet;


        public void startCreateContents()
        {

            var sheets = settings.sheetsToContents;

            dynamic[,] arrayContents = new dynamic[sheets.Count + 1, 3];

            int numberOfContent = 1;
            foreach (var sheetInfo in sheets)
            {

                int sheetId = sheetInfo.SheetNumber;
                var sheet = ExcelFunctions.GetSheetByIndex(sheetId);

                //порядковый номер
                arrayContents[numberOfContent, 0] = numberOfContent.ToString();

                //откуда взять имя
                //в зависимости от настроек, либо из имени листа, либо из ячейки
                string shName = settings.isNameFromSheet ? sheetInfo.SheetName : sheet.Range[settings.cellRangeForContentsName].Value?.ToString();
                shName = String.IsNullOrWhiteSpace(shName) ? "ПУСТО" : shName;

                //пишем формулу в оглавление
                arrayContents[numberOfContent, 1] = $@"=HYPERLINK(""#'{sheetInfo.SheetName}'!A1"",""{shName}"")";

                //считаем количество значимых строк
                if (settings.isCountQuantityOfRows) 
                    arrayContents[numberOfContent, 2] = (sheet.UsedRange.Row + sheet.UsedRange.Rows.Count);

                //ссоздаем ссылку "К оглавлению"
                if (settings.isCreateLinkToContentsOnAllSheets)
                    ExcelFunctions.CreateLinkToContentsSheet(sheet, "Оглавление");

                numberOfContent++;
            }

            //пишем заголовки
            arrayContents[0, 0] = "№";
            arrayContents[0, 1] = "Ссылка на лист";
            arrayContents[0, 2] = settings.isCountQuantityOfRows? "Количество строк на листе" : "";

            Excel.Worksheet contentSheet = ExcelFunctions.GetOrCreateSheet("Оглавление");

            //очищаем лист оглавление если указано в настройках
            if (settings.isCleanContentsSheet) contentSheet.UsedRange.Value = "";

            contentSheet.Range["B2:D" + (arrayContents.GetLength(0) + 1)].Value = arrayContents;
            //contentSheet.Range["C2:C" + (arrayContents.GetLength(0) + 1)].Formula = contentSheet.Range["C2:C" + (arrayContents.GetLength(0) + 1)].Value;

        }

        public class Settings
        {
            public List<SheetInList> sheetsToContents { get; set; }

            public bool isNameFromSheet { get; set; }

            public string cellRangeForContentsName { get; set; }

            public bool isCreateLinkToContentsOnAllSheets { get; set; }

            public bool isCountQuantityOfRows { get; set; }

            public bool isCleanContentsSheet { get; set; }
        }

        public class SheetInList
        {
            public bool CheckedSheet { get; set; }

            public int SheetNumber { get; set; }

            public string SheetName { get; set; }

        }
    }
}
