﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;

namespace WebtronicsAddIn.Tools.OnRibbon.About
{
    public class AddinLicense
    {
        public string ClientEmail { get; set; }
        public string LicenseKey { get; set; }
        public string Promocode { get; set; }
        public string LicenseType { get; set; }
        public bool IsLicenseActive { get; set; }
        public DateTime LicenseDateTo { get; set; }
        public DateTime LicenseCheckedDate { get; set; }
        public string HID { get; set; }

        public static bool? CheckLicense(HttpClient httpClient)
        {
            var mySettings = Properties.Settings.Default;
            var today = DateTime.Now;
            if ((today - mySettings.LicenseCheckedDate).Days > 3) LicenseCheckoutMaster(httpClient);
            if (!mySettings.IsLicenseActive) return false;
            if (today.CompareTo(mySettings.LicenseDateTo) > 0)
            {
                mySettings.IsLicenseActive = false;
                mySettings.Save();
                return false;
            }
            if ((today - mySettings.LicenseCheckedDate).Days > 7) return null;
            return true;
        }

        public void getDataFromForm(AboutForm form)
        {
            ClientEmail = form.ClientMailTextbox.Text;
            LicenseKey = form.LicenseKeyTextbox.Text;
            Promocode = form.PromocodeTextbox.Text;

            HID = Properties.Settings.Default.HID;
        }

        public void getDataFromSettings()
        {
            var settings = Properties.Settings.Default;
            ClientEmail = settings.ClientMail;
            LicenseKey = settings.LicenseKey;
            Promocode = settings.Promocode;
            HID = settings.HID;
        }

        public static bool IsLicenseNotActive(HttpClient httpClient)
        {
            bool? isActive = CheckLicense(httpClient);


            if (isActive is null)
            {
                MessageBox.Show("Последняя проверка лицензии была > 7 дней назад! Подключитесь к интернету и проверьте лицензию!");
                return false;
            }

            if (!(bool)isActive)
            {
                MessageBox.Show("Необходимо приобрести лицензию для использования продукта!", "Не найдена лицензия!");
                return false;
            }
            return true;
        }

        public void askForTrial(HttpClient httpClient)
        {
            getTrialFromServer(httpClient);
        }

        private async void getTrialFromServer(HttpClient httpClient)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            var response = await httpClient.PostAsync(
                "https://tools.webtronics.ru/excel/get-trial",
                 GetContentForTrial());
            if (!response.IsSuccessStatusCode)
            {
                MessageBox.Show("Ошибка подключения к серверу лицензирования!");
                return;
            }
            var jsonString = await response.Content.ReadAsStringAsync();

            var ResponseDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString);

            if (ResponseDict.ContainsKey("error"))
            {
                MessageBox.Show(ResponseDict["error"]);
                return;
            }

            WriteLicenseInfo(ResponseDict);

            MessageBox.Show("Триальная лицензия успешно активирована!");
        }

        public async void LicenseCheckout(HttpClient httpClient, bool isShowMessages = false)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            HttpResponseMessage response;
            try { 
            response = await httpClient.PostAsync(
                "https://tools.webtronics.ru/excel/license-checkout",
                 GetContentForLicenseCheckout());
            } catch (Exception)
            {
                if (isShowMessages) MessageBox.Show("Ошибка подключения к серверу лицензирования!");
                return;
            }

            if (!response.IsSuccessStatusCode)
            {
                if (isShowMessages) MessageBox.Show("Ошибка ответа от сервера лицензирования!"); 
                return;
            }

            var jsonString = await response.Content.ReadAsStringAsync();
            var ResponseDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString);

            if (ResponseDict.ContainsKey("error"))
            {
                if (isShowMessages) MessageBox.Show(ResponseDict["error"]);
                return;
            }

            WriteLicenseInfo(ResponseDict);

            if (isShowMessages) MessageBox.Show("Лицензионный ключ с заданными параметрами найден!");
        }

        private static void WriteLicenseInfo(Dictionary<string, string> response)
        {
            var settings = Properties.Settings.Default;

            settings.LicenseKey = response["key"];
            settings.LicenseType = response["type"];
            settings.Promocode = response["promo_code"];
            settings.LicenseDateTo = Convert.ToDateTime(response["date_to"]);
            settings.LicenseCheckedDate = Convert.ToDateTime(response["date_checked"]);
            settings.LicenseDateCreated = Convert.ToDateTime(response["date_created"]);
            settings.ClientMail= response["client_email"];

            settings.IsLicenseActive = Convert.ToBoolean(response["active"]) && settings.LicenseDateTo.CompareTo(DateTime.Now) > 0 ;

            settings.Save();
        }


        private FormUrlEncodedContent GetContentForTrial()
        {
            return new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("auth", "addin-lic-trial"),
                    new KeyValuePair<string, string>("client_email", ClientEmail),
                    new KeyValuePair<string, string>("hid", HID),
                    new KeyValuePair<string, string>("promocode", Promocode),
                });

        }

        private FormUrlEncodedContent GetContentForLicenseCheckout()
        {
            return new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("auth", "addin-lic-check"),
                    new KeyValuePair<string, string>("client_email", ClientEmail),
                    new KeyValuePair<string, string>("hid", HID),
                    new KeyValuePair<string, string>("key", LicenseKey),
                });

        }


        public static void LicenseCheckoutMaster(HttpClient httpClient)
        {
            var lic = new AddinLicense();
            lic.getDataFromSettings();
            lic.LicenseCheckout(httpClient, true);
        }

        public static string getHardwareId()
        {
            var mbs = new ManagementObjectSearcher("Select ProcessorId From Win32_processor");
            ManagementObjectCollection mbsList = mbs.Get();
            string id = "";
            foreach (ManagementObject mo in mbsList)
            {
                id = mo["ProcessorId"].ToString();
                break;
            }

            mbs = new ManagementObjectSearcher("SELECT SerialNumber FROM Win32_BaseBoard");
            mbsList = mbs.Get();

            foreach (ManagementObject mo in mbsList)
            {
                id += (string)mo["SerialNumber"];
            }

            return id;

        }


        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}
