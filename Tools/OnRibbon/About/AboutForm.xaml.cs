﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;

namespace WebtronicsAddIn.Tools.OnRibbon.About
{
    /// <summary>
    /// Логика взаимодействия для AboutForm.xaml
    /// </summary>
    public partial class AboutForm : Window
    {
        public HttpClient httpClient;

        public AboutForm(HttpClient _httpClient)
        {
            InitializeComponent();
            AboutUs_Hyperlink.RequestNavigate += LinkOnRequestNavigate;
            TelegramChannel_Hyperlink.RequestNavigate += LinkOnRequestNavigate;
            var settings = Properties.Settings.Default;
            if (settings.HID.Length < 1)
            {
                settings.HID = AddinLicense.getHardwareId();
                settings.Save();
            }

            if (Properties.Settings.Default.IsLicenseActive)
            {
                GetTrialButton.IsEnabled = false;
                ActivateButton.Content = "Обновить статус";
            }

            httpClient = _httpClient;
        }



        private void LinkOnRequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.ToString());
        }

        private void ActivateButton_Click(object sender, RoutedEventArgs e)
        {
            if (!isRequieredFieldsOk(new string[] { ClientMailTextbox.Text, LicenseKeyTextbox.Text })) return;
            var lic = new AddinLicense();
            lic.getDataFromForm(this);
            lic.LicenseCheckout(httpClient, true);
        }

        private void GetTrialButton_Click(object sender, RoutedEventArgs e)
        {
            if (!isRequieredFieldsOk(new string[] { ClientMailTextbox.Text})) return;
            var lic = new AddinLicense();
            lic.getDataFromForm(this);
            lic.askForTrial(httpClient);
        }

        private bool isRequieredFieldsOk(string[] fields)
        {
            foreach (string item in fields)
            {
                if (item.Length < 1)
                {
                    MessageBox.Show("Заполните необходимые поля!");
                    return false;
                }
            }
            return true;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
