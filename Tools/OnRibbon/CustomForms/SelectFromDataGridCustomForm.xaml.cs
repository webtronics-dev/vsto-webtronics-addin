﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;

namespace WebtronicsAddIn.Tools.OnRibbon.CustomForms
{
    /// <summary>
    /// Логика взаимодействия для SelectFromDataGridCustomForm.xaml
    /// </summary>
    public partial class SelectFromDataGridCustomForm : Window
    {
        CollectionViewSource _viewSource { get; set; }
        List<ExcelClasses.Cell> _selectedColumns { get; set; }
        public SelectFromDataGridCustomForm(CollectionViewSource viewSource, List<ExcelClasses.Cell> selectedColumns)
        {
            InitializeComponent();
            _viewSource = viewSource;
            _selectedColumns = selectedColumns;

            customList.ItemsSource = this._viewSource.View;
        }

        private void RunButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedSource = _viewSource.Source as IEnumerable<ExcelClasses.Cell>;
            ExcelClasses.Cell column = selectedSource.FirstOrDefault(col => col.IsChecked == true);
            if (column is null)
            {
                MessageBox.Show("Выберите значение!");
                return;
            }
            
            if (_selectedColumns.Contains(column))
            {
                MessageBox.Show("Значение уже присутствует в списке выбранных! Выберите другое!");
                return;
            }

            _selectedColumns.Add(column);
            Close();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
