﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using WebtronicsAddIn.Tools.Functions;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.FilterToNewSheet
{
    public class FilterToNewSheetMainLogic
    {
        Settings settings { get; set; }
        public class Settings
        {
            public HashSet<String> valuesToFilter { get; set; }
            public int headerRow { get; set; }
            public string columnLetter { get; set; }
            public int columnNumber { get; set; }
            public Excel.Worksheet sheet { get; set; }
            public bool isCopyHeader { get; set; }
        }

        Dictionary<string, int>  existingSheets { get; set; }

        public FilterToNewSheetMainLogic(Settings _settings)
            {
            settings = _settings;
            }


        public void startScript()
        {

            string[,] headerRow = new string[0,0];
            

            var arrAllVals = ExcelFunctions.GetStringArrayFromRange(ExcelFunctions.GetAllSheetRange(settings.sheet));

            if (settings.isCopyHeader) headerRow = getHeaderRow(settings.headerRow - 1, arrAllVals);

            var sheets = ExcelFunctions.GetAllSheetsOfActiveWorkbook();
            existingSheets = ExcelFunctions.CreateDictionaryOfSheetsKeyName(sheets);

            foreach (string selectedValue in settings.valuesToFilter)
            {

                var filteredArray = ExcelFunctions.GetFilteredArrayFromStringArray(arrAllVals, selectedValue, settings.columnNumber - 1);

                string shName = selectedValue;

                if (existingSheets.ContainsKey(shName)) shName = shName + "(" + existingSheets.Count + ")";
                existingSheets.Add(shName, existingSheets.Count + 1);

                var sheet = ExcelFunctions.AddSheetAfter(shName, existingSheets.Count - 1);

                if (settings.isCopyHeader)
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[1, filteredArray.GetLength(1)]].Value = headerRow;
                    sheet.Range[sheet.Cells[2, 1], sheet.Cells[filteredArray.GetLength(0) + 1, filteredArray.GetLength(1)]].Value = filteredArray;
                } else
                {
                    sheet.Range[sheet.Cells[1, 1], sheet.Cells[filteredArray.GetLength(0), filteredArray.GetLength(1)]].Value = filteredArray;
                }



            }
        }

        private string[,] getHeaderRow (int row, string[,] arrValues)
        {
            int secondDimension = arrValues.GetLength(1);
            string[,] arrHeader = new string[1, secondDimension];

            for (int i = 0; i < secondDimension; i++)
            {
                arrHeader[0, i] = arrValues[row, i];
            }

            return arrHeader;

        }

    }


}
