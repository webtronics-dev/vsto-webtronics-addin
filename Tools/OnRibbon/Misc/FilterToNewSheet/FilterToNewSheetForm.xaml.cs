﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.FilterToNewSheet
{
    /// <summary>
    /// Логика взаимодействия для FilterToNewSheetForm.xaml
    /// </summary>
    public partial class FilterToNewSheetForm : Window
    {

        public int columnNumber;
        public int headerRow;
        public string columnLetter;
        Excel.Worksheet sheet;
        private Excel.Application ExcelApp = Globals.ThisAddIn.Application;

        public ObservableCollection<cellValue> collectionOfValues;

        public FilterToNewSheetForm()
        {
            InitializeComponent();
            Excel.Range cell = ExcelFunctions.GetActiveCell();
            if (cell is null) return;
            columnLetterTextBox.Text = ExcelFunctions.ExcelColumnFromNumber(cell.Column);
        }


        CollectionViewSource viewSource;
        void viewSource_Filter(object sender, FilterEventArgs e)
        {
            cellValue myValue = e.Item as cellValue;
            e.Accepted = (myValue.value).IndexOf(filter.Text) >= 0;
        }
        public ObservableCollection<cellValue> CreateObservibleUniq(HashSet<string> uniqValues)
        {

            var ObColl = new ObservableCollection<cellValue>();
            foreach (var item in uniqValues)
            {
                if (item != null)
                    ObColl.Add(new cellValue() { value = item, IsChecked = false });
            }

            return ObColl;
        }

        public class cellValue
        {
            public string value { get; set; }

            public bool IsChecked { get; set; }
        }


        private void filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            viewSource.View.Refresh();
        }

        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {

            foreach (var item in valuesGrid.Items)
            {
                var tmp = item as cellValue;
                tmp.IsChecked = true;
            }

            viewSource.View.Refresh();

        }

        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var item in valuesGrid.Items)
            {
                var tmp = item as cellValue;
                tmp.IsChecked = false;
            }

            viewSource.View.Refresh();
        }

        private void CloseButt_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void applySettings_Click(object sender, RoutedEventArgs e)
        {

            sheet = ExcelFunctions.GetActiveWorksheet();
            columnLetter = columnLetterTextBox.Text;
            columnNumber = ExcelFunctions.NumberFromExcelColumn(columnLetter);
            if ((bool)isHeaderEnabled.IsChecked)
            {
                headerRow = Int32.Parse(headerRowTextBox.Text);
            }
            else
            {
                headerRow = 0;
            }

            HashSet<string> uniqValues;
            try
            {
                uniqValues = ExcelFunctions.GetUniqValuesFromColumn(sheet, columnNumber, headerRow);
            }
            catch
            {
                MessageBox.Show("Ошибка в выбранном диапазоне!");
                return;
            }
            collectionOfValues = CreateObservibleUniq(uniqValues);
            viewSource = new CollectionViewSource();
            viewSource.Source = collectionOfValues;
            viewSource.Filter += viewSource_Filter;
            valuesGrid.ItemsSource = viewSource.View;
        }

        private void headerRowTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsNumber);
        }

        private void headerRowTextBox_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsNumber))
                e.CancelCommand();
        }

        bool IsNumber(char c)
        {
            if (c >= '0' && c <= '9')
                return true;
            return false;
        }


        bool IsLetter(char c)
        {
            if (c >= 'a' && c <= 'f')
                return true;
            if (c >= 'A' && c <= 'F')
                return true;
            return false;
        }

        private void columnLetterTextBox_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsLetter))
                e.CancelCommand();
        }

        private void columnLetterTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsLetter);
        }

        private void runScript_Click(object sender, RoutedEventArgs e)
        {



            var settings = new FilterToNewSheetMainLogic.Settings
            {
                columnLetter = columnLetter,
                columnNumber = columnNumber,
                headerRow = headerRow,
                valuesToFilter = getSetOfCheckedValues(),
                sheet = sheet,
                isCopyHeader = (bool)isHeaderEnabled.IsChecked
            };

            if (settings.valuesToFilter.Count < 1) return;

            FilterToNewSheetMainLogic main = new FilterToNewSheetMainLogic(settings);
            WaitTextBlock.Visibility = Visibility.Visible;
            runButton.IsEnabled = false;
            ExcelApp.ScreenUpdating = false;
            main.startScript();
            ExcelApp.ScreenUpdating = true;
            MessageBox.Show("Готово!", "Готово!");

            Close();
        }

        private HashSet<String> getSetOfCheckedValues()
        {
            var valuesSet = new HashSet<string>();
            var listCheckedValues = valuesGrid.Items
                    .Cast<cellValue>()
                    .Where(item => item.IsChecked);
            foreach (var item in listCheckedValues)
            {
                valuesSet.Add(item.value);
            }

            return valuesSet;
        }


    }
}
