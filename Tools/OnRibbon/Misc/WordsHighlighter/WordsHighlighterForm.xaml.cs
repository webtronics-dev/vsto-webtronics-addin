﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.WordsHighlighter
{
    /// <summary>
    /// Логика взаимодействия для WordsHighlighterForm.xaml
    /// </summary>
    public partial class WordsHighlighterForm : Window
    {
        public WordsHighlighterForm()
        {
            InitializeComponent();
        }

        private void CloseButt_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ChooseRange2_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            var rng = ExcelFunctions.SelectRange();
            this.Show();
            if (rng is null) return;
            rng2_Textbox.Text = $"'{rng.Worksheet.Name}'!{rng.Address}";
        }

        private void ChooseRange1_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            var rng = ExcelFunctions.SelectRange();
            this.Show();
            if (rng is null) return;
            rng1_Textbox.Text = $"'{rng.Worksheet.Name}'!{rng.Address}";
        }

        private void RunScript_Click(object sender, RoutedEventArgs e)
        {
            if (!IsRequiredFieldsOk())
            {
                MessageBox.Show("Заполните необходимые поля!");
                return;
            }
            var rng1 = ExcelFunctions.GetRangeFromString(rng1_Textbox.Text);
            var rng2 = ExcelFunctions.GetRangeFromString(rng2_Textbox.Text);

            var sheetToHighlight = ExcelFunctions.GetWorksheetFromStringRangeAnnotation(rng2_Textbox.Text);
            

            var valuesToLook = new ExcelClasses.Cells(rng1).GetDictionaryOfUniqCells();
            var cellsToHighlight = new ExcelClasses.Cells(rng2);

            var color = ExcelFunctions.ConvertMediaColorToOle(HighlightColor_ColorPicker.Color);

            foreach (var cell in cellsToHighlight.list)
            {
                if (cell.stringValue != null && cell.stringValue.Length > 0)
                {
                    string cellValue = cell.stringValue.ToLower();
                    var neededValuesInCell = valuesToLook.Keys.Where(x => cellValue.Contains(x.ToLower())).ToList();
                    if (neededValuesInCell.Count > 0) sheetToHighlight.Range[cell.getCellAddress()].Interior.Color = color;
                }
            }


        }

        private bool IsRequiredFieldsOk()
        {
            if (rng1_Textbox.Text.Length < 3 || rng2_Textbox.Text.Length < 3) return false;
            return true;
        }
    }
}
