﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.OnRibbon.ContentsNS;
using Excel = Microsoft.Office.Interop.Excel;
namespace WebtronicsAddIn.Tools.OnRibbon.Misc.BooksMerge
{
    /// <summary>
    /// Логика взаимодействия для BooksMergeForm.xaml
    /// </summary>
    public partial class BooksMergeForm : Window, INotifyPropertyChanged
    {
        private Excel.Application ExcelApp = Globals.ThisAddIn.Application;

        public string _ProgressText = "Ожидайте! Операция выполняется!";

        public string ProgressText
        {
            get { return _ProgressText; }
            set { 
                _ProgressText = value;
                OnPropertyChanged("ProgressText");
            }
        }
        


        public BooksMergeForm()
        {
            InitializeComponent();
            myCollection = new ObservableCollection<gridValue>();
        }

        private void CloseButt_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void runScript_Click(object sender, RoutedEventArgs e)
        {

            var checkedBooks = getCheckedBooksFilePaths();
            if (checkedBooks.Count < 1)
            {
                MessageBox.Show("Выберите хотя бы одну книгу!", "Ошибка");
                return;
            }
            ProgressTextBlock.Visibility = Visibility.Visible;
            RunScriptButton.Visibility = Visibility.Hidden;
            this.Topmost = true;
            ExcelApp.ScreenUpdating = false;

            BookMergeMainLogic script = new BookMergeMainLogic(checkedBooks, ProgressText, 
                (bool)isNetPeakSpider.IsChecked, (bool)isCreateContents_CheckBox.IsChecked, (bool)isChangeSheetName_CheckBox.IsChecked);

            if ((bool)isCreateContents_CheckBox.IsChecked) script.ContentsSettings = GetContentsSettings();

            await Task.Run(() => script.startScript());
            ExcelApp.ScreenUpdating = true;
            this.Topmost = false;
            MessageBox.Show("Готово!", "Готово!");

            Close();

        }
        
        private ContentsCreator.Settings GetContentsSettings()
        {
            var settings = new ContentsCreator.Settings
            {
                isNameFromSheet = true,
                isCreateLinkToContentsOnAllSheets = (bool)isCreateLinkToContentsOnAllSheets_CheckBox.IsChecked,
                isCountQuantityOfRows = (bool)isCountQuantityOfRows_CheckBox.IsChecked,
            };
            return settings;
        }

        private List<string> getCheckedBooksFilePaths()
        {
            List<string> checkedBooks = new List<string>();

            foreach (var item in myCollection)
            {
                if (item.IsChecked) checkedBooks.Add(item.filePath);
            }

            return checkedBooks;
        }

        private void chooseBooksButton_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.Multiselect = true;
            openFileDlg.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            Nullable<bool> result = openFileDlg.ShowDialog();

            if (result == true)
            {
                var arrayFiles = openFileDlg.FileNames;

                CreateObservibleCollection(arrayFiles);

                viewSource = new CollectionViewSource();
                viewSource.Source = myCollection;
                viewSource.Filter += viewSource_Filter;
                valuesGrid.ItemsSource = viewSource.View;

            }
        }


        //DATA GRID collections


        public ObservableCollection<gridValue> myCollection;


        CollectionViewSource viewSource;
        void viewSource_Filter(object sender, FilterEventArgs e)
        {

            gridValue myValue = e.Item as gridValue;

            e.Accepted = (myValue.fileName).IndexOf(filter.Text) >= 0;
        }
        public void CreateObservibleCollection(string[] values)
        {

            //var ObColl = new ObservableCollection<gridValue>();
            foreach (var item in values)
            {
                if (item != null)
                    myCollection.Add(new gridValue() 
                    { 
                        filePath = item, 
                        fileName = System.IO.Path.GetFileName(item),
                        IsChecked = true 
                    });
              }

        //    return ObColl;
        }

        public class gridValue
        {
            public string filePath { get; set; }
            public string fileName { get; set; }

            public bool IsChecked { get; set; }
        }


        private void filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            viewSource.View.Refresh();
        }

        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {

            foreach (var item in valuesGrid.Items)
            {
                var tmp = item as gridValue;
                tmp.IsChecked = true;
            }

            viewSource.View.Refresh();

        }

        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var item in valuesGrid.Items)
            {
                var tmp = item as gridValue;
                tmp.IsChecked = false;
            }

            viewSource.View.Refresh();
        }

        private void removeSelectedButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in myCollection.ToArray())
            {
                if (item.IsChecked) myCollection.Remove(item);
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected void OnPropertyChanged(string propertyName = null)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
        #endregion
    }


}
