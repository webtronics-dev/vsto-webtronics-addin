﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using WebtronicsAddIn.Tools.OnRibbon.ContentsNS;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.BooksMerge
{
    public class BookMergeMainLogic
    {
        public BooksPaths booksPaths { get; set; }
        public ContentsCreator.Settings ContentsSettings { get; set; }
        private bool IsCreateContents { get; set; }
        private bool IsSheetNameFromBookName { get; set; }

        public BookMergeMainLogic (List<string> _filesToMerge, string _ProgressText, bool _IsForNetpeakSpider, bool _IsCreateContents, bool _isSheetNameFromBookName)
        {
            booksPaths = new BooksPaths(_filesToMerge);
            if (_IsForNetpeakSpider) booksPaths.ChangeBooksOrderForNetPeak();
            IsCreateContents = _IsCreateContents;
            IsSheetNameFromBookName = _isSheetNameFromBookName;
        }

        public void startScript()
        {
            var mainBook = ExcelFunctions.AddNewWorkbook();
            mainBook.Application.ScreenUpdating = false;
            mainBook.Application.DisplayAlerts = false;

            foreach (var pair in booksPaths.Books)
            {
                var additionalBook = ExcelFunctions.OpenWorkbook(pair.Key);
                foreach (Excel.Worksheet sheet in additionalBook.Sheets)
                {
                    //если нужно имя листа из имени книги, изменяем имя листа
                    if (IsSheetNameFromBookName)
                    {
                        var newShName = ExcelFunctions.CleanStringForSheetName(additionalBook.Name);
                        if (additionalBook.Sheets.Count > 1 && ExcelFunctions.IsSheetExist(newShName, additionalBook)) 
                        {
                            newShName = newShName.Substring(0, 23) + "(" + additionalBook.Sheets.Count + ")"; 
                        }
                        sheet.Name = newShName;
                    }
                    sheet.Copy(After:mainBook.Sheets[mainBook.Sheets.Count]);
                    additionalBook.Close();
                }
            }

            ((Excel.Worksheet)mainBook.Sheets[1]).Delete(); //удаляем первый пустой лист, созданный при инициализации новой книги
            if (IsCreateContents) CreateContents(mainBook);


            mainBook.Application.DisplayAlerts = true;
            mainBook.Application.ScreenUpdating = true;

            mainBook.Activate();

        }


        private void CreateContents(Excel.Workbook wb)
        {
            //создаем список листов для оглавления
            var sheets = new List<ContentsCreator.SheetInList>();
            foreach (Excel.Worksheet sheet in wb.Sheets) sheets.Add(new ContentsCreator.SheetInList() { CheckedSheet = true, SheetNumber = sheet.Index, SheetName = sheet.Name });
            ContentsSettings.sheetsToContents = sheets;
            //создаем и пишем оглавление
            var contents = new ContentsCreator(ContentsSettings);
            contents.startCreateContents();
        }

        //класс хранит пути к книгам для объединения и порядок объединения
        public class BooksPaths
        {
            public Dictionary<string, int> Books { get; set; }
            public BooksPaths(List <string> _books)
            {
                Books = _books.ToDictionary(b => b, b => 99);
            }

            public void ChangeBooksOrderForNetPeak()
            {
                Dictionary<string, int> newOrder = new Dictionary<string, int>();

                var NetPeakOrder = new Dictionary<string, int>{
                    {"Битые", 1},
                    {"ВК", 2},
                    {"СК", 3},
                    {"НК", 4},
                    };
            
                foreach (KeyValuePair<string, int> book in Books)
                {
                    bool found = false;
                    foreach (KeyValuePair<string, int> order in NetPeakOrder)
                    {
                        string bookName = book.Key.Substring(book.Key.LastIndexOf("\\") + 1);
                        if (bookName.StartsWith(order.Key))
                        {
                            newOrder[book.Key] = order.Value;
                            found = true;
                            break;
                        }
                    }
                    if (!found) newOrder[book.Key] = book.Value;
                }

                Books = newOrder.OrderBy(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            }
        }

    }


}
