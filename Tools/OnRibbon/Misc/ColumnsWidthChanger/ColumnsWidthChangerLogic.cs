﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.ColumnsWidthChanger
{
    class ColumnsWidthChangerLogic
    {
        public ColumnsWidthChangerForm.Settings settings { get; set; }

        public ColumnsWidthChangerLogic(ColumnsWidthChangerForm.Settings formSettings)
        {
            settings = formSettings;
        }

        public void RunScript()
        {

            var activeSheetRng = ExcelFunctions.GetActiveWorksheet().UsedRange;
            activeSheetRng.Copy();
            foreach (ExcelSheets.MySheet mySheet in settings.selectedSheets)
            {

                if (settings.IsAllColumnsSelected && settings.IsWidthLikeActiveSheet)
                {
                    var sheetRange = mySheet.SheetRef.UsedRange;
                    sheetRange.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteColumnWidths);
                }
                else
                {
                    settings.Columns.SetColumnsWidthToSheet(mySheet.SheetRef, IsOnlyCheckedColumns: true);
                }

                if (settings.IsChangeRowHeight && settings.IsHeightAuto) mySheet.SheetRef.UsedRange.EntireRow.AutoFit();
                if (settings.IsChangeRowHeight && !settings.IsHeightAuto) mySheet.SheetRef.UsedRange.EntireRow.RowHeight = settings.CustomRowHeight;

            }
        }
    }
}

