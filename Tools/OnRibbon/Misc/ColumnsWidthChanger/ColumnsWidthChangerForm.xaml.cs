﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.ColumnsWidthChanger
{
    /// <summary>
    /// Логика взаимодействия для ColumnsWidthChangerForm.xaml
    /// </summary>
    public partial class ColumnsWidthChangerForm : Window
    {
        public CollectionViewSource sheetsCollection;
        public ExcelSheets.SheetsList sheets;
        private Excel.Application ExcelApp = Globals.ThisAddIn.Application;

        public Settings settings = new Settings();

        public class Settings
        {
            public CollectionViewSource ColumnsCollection { get; set; }
            public ExcelClasses.Columns Columns { get; set; }
            public string CustomColumnsRangeString { get; set; }
            public bool IsAllColumnsSelected { get; set; }

            public bool IsWidthLikeActiveSheet { get; set; }
            public bool IsHeightAuto { get; set; }
            public bool IsChangeRowHeight { get; set; }

            public float CustomColumnWidth;
            public float CustomRowHeight;

            public IEnumerable<ExcelSheets.MySheet> selectedSheets;
            public IEnumerable<ExcelClasses.Column> selectedColumns;

        }

        public ColumnsWidthChangerForm()
        {
            InitializeComponent();
            sheets = new ExcelSheets.SheetsList(ExcelSheets: ExcelFunctions.GetAllSheetsOfActiveWorkbook(), IsChecked: true);
            sheets.list[0].IsChecked = false;

            sheetsCollection = sheets.CreateCollectionViewSource();
            sheetsCollection.Filter += viewSourceSheet_Filter;
            sheetsGrid.ItemsSource = sheetsCollection.View;

            settings.Columns = new ExcelClasses.Columns(ExcelFunctions.GetLastColumnInSheet(ExcelFunctions.GetActiveWorksheet()), IsCheckedColumns: true);

            settings.ColumnsCollection = settings.Columns.createCollectionViewSource();
            columnsList.ItemsSource = settings.ColumnsCollection.View;

        }

        private async void RunButton_Click(object sender, RoutedEventArgs e)
        {
            bool IsSettingsOk = GetFormSettings();
            if (!IsSettingsOk) return;
            RunButton.IsEnabled = false;
            ExcelApp.ScreenUpdating = false;
            var script = new ColumnsWidthChangerLogic(settings);
            await Task.Run(() => script.RunScript());
            ExcelApp.ScreenUpdating = true;
            MessageBox.Show("Готово!");
            Close();
        }

        private bool GetFormSettings()
        {
            settings.IsHeightAuto = (bool)IsHeightAuto_RadioButton.IsChecked;
            settings.IsWidthLikeActiveSheet = (bool)IsWidthLikeActiveSheet_RadioButton.IsChecked;
            settings.IsChangeRowHeight = (!(bool)IsNotChangeRowHeight_RadioButton.IsChecked);

            if (!settings.IsHeightAuto)
            { 
                if (!float.TryParse(CustomRowHeight_Textbox.Text, out settings.CustomRowHeight))
                {
                    MessageBox.Show("Исправьте ошибку в высоте строк!");
                    return false;
                };
            }
            if (!settings.IsWidthLikeActiveSheet)
            {
                if (!float.TryParse(CustomColumnsWidth_Textbox.Text, out settings.CustomColumnWidth)) 
                {
                    MessageBox.Show("Исправьте ошибку в ширине столбцов!");
                    return false; 
                };
            }

            settings.selectedSheets = from sheet in sheetsCollection.Source as IEnumerable<ExcelSheets.MySheet>
                                      where sheet.IsChecked
                                      select sheet;

            if (settings.selectedSheets.Count() < 1)
            {
                MessageBox.Show("Выберите хотя бы один лист!");
                return false;
            }

            settings.selectedColumns = from col in settings.ColumnsCollection.Source as IEnumerable<ExcelClasses.Column>
                                      where col.IsChecked
                                      select col;

            if (settings.selectedColumns.Count() < 1)
            {
                MessageBox.Show("Выберите хотя бы один столбец!");
                return false;
            }

            if (settings.selectedColumns.Count() != settings.Columns.list.Length)
            {
                settings.IsAllColumnsSelected = false;
            } else
            {
                settings.IsAllColumnsSelected = true;
            }

            if (settings.IsWidthLikeActiveSheet)
            {
                settings.Columns.GetColumnsWidthFromSheet();
            } else
            {
                settings.Columns.SetWidthToColumns(settings.CustomColumnWidth);
            }


            return true;
        }


        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var sheet in sheets.list) sheet.IsChecked = true;
            sheetsCollection.View.Refresh();
        }

        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var sheet in sheets.list) sheet.IsChecked = false;
            sheetsCollection.View.Refresh();
        } 
        private void selectAllColumns_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var col in settings.Columns.list) col.IsChecked = true;
            settings.ColumnsCollection.View.Refresh();
        }

        private void selectAllColumns_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var col in settings.Columns.list) col.IsChecked = false;
            settings.ColumnsCollection.View.Refresh();
        }

        private void viewSourceSheet_Filter(object sender, FilterEventArgs e)
        {
            ExcelSheets.MySheet sheet = e.Item as ExcelSheets.MySheet;
            e.Accepted = sheet.Name.IndexOf(filterSheets.Text) >= 0;
        }

        private void filterSheets_TextChanged(object sender, TextChangedEventArgs e)
        {
            sheetsCollection.View.Refresh();
        }
        private void filterColumns_TextChanged(object sender, TextChangedEventArgs e)
        {
            settings.ColumnsCollection.View.Refresh();
        }
        private void Number_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsNumberOrFloat);
        }
        private void Number_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsNumberOrFloat))
                e.CancelCommand();
        }

        bool IsNumberOrFloat(char c)
        {
            if ((c >= '0' && c <= '9') || (c == ','))
                return true;
            return false;
        }

    }
}
