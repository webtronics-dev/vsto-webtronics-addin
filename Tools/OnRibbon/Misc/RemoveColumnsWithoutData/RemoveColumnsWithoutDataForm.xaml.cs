﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.RemoveColumnsWithoutData
{
    /// <summary>
    /// Логика взаимодействия для RemoveColumnsWithoutDataForm.xaml
    /// </summary>
    public partial class RemoveColumnsWithoutDataForm : Window
    {
        private CollectionViewSource sheetsCollection;
        private ExcelSheets.SheetsList sheets;
        private Excel.Application ExcelApp = Globals.ThisAddIn.Application;
        private IEnumerable<ExcelSheets.MySheet> selectedSheets;
        private bool IsAllColumnShouldBeEmpty { get; set; }
        private int RowAfterColumnIsEmpty;

        public RemoveColumnsWithoutDataForm()
        {
            InitializeComponent();
            sheets = new ExcelSheets.SheetsList(ExcelSheets: ExcelFunctions.GetAllSheetsOfActiveWorkbook(), IsChecked: false);
            sheets.list[0].IsChecked = true;

            sheetsCollection = sheets.CreateCollectionViewSource();
            sheetsCollection.Filter += viewSourceSheet_Filter;
            sheetsGrid.ItemsSource = sheetsCollection.View;
        }

        private void RunButton_Click(object sender, RoutedEventArgs e)
        {
            if (!IsSettingsOk()) return;
            int deletedCounter = 0;
            ExcelApp.ScreenUpdating = false;
            foreach (ExcelSheets.MySheet sheet in selectedSheets)
            {
                dynamic[,] values;
                var columnsToDelete = new List<string>();
                if (IsAllColumnShouldBeEmpty) values = ExcelFunctions.GetSheetValues(sheet.SheetRef); else values = ExcelFunctions.GetSheetValues(sheet.SheetRef, RowAfterColumnIsEmpty);
                if (values is null) continue ;

                for (int col = 1; col <= values.GetLength(1); col++)
                {
                    bool shouldDeleteColumn = true;
                    for (int row = 1; row <= values.GetLength(0); row++)
                    {
                        if (values[row, col] != null && Convert.ToString(values[row, col]).Trim().Length > 0)
                        {
                            shouldDeleteColumn = false;
                            break;
                        }
                    }
                    if (shouldDeleteColumn)
                    {
                        columnsToDelete.Add(ExcelFunctions.ExcelColumnFromNumber(col) + "1");
                        deletedCounter++;
                    }
                }

                columnsToDelete.Reverse();
                string columnsRange = "";
                string separatorColumns = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;

                for (int i = 0; i < columnsToDelete.Count; i++)
                {
                    columnsRange += (columnsToDelete[i] + separatorColumns);
                    if (i != 0 && i % 50 == 0) DeleteColumnsRange(ref columnsRange, sheet.SheetRef);

                }
                if (columnsRange.Length > 0) DeleteColumnsRange(ref columnsRange, sheet.SheetRef);
            }
            ExcelApp.ScreenUpdating = true;
            MessageBox.Show($"Готово! Всего удалено столбцов: {deletedCounter}");
            Close();
        }

        private void DeleteColumnsRange(ref string columnsRange, Excel.Worksheet sheet)
        {
            columnsRange = columnsRange.Substring(0, columnsRange.Length - 1);
            var rng = sheet.Range[columnsRange].EntireColumn;
            rng.Delete();
            columnsRange = "";
        }
        private bool IsSettingsOk()
        {
            selectedSheets = from sheet in sheetsCollection.Source as IEnumerable<ExcelSheets.MySheet>
                                      where sheet.IsChecked
                                      select sheet;

            if (selectedSheets.Count() < 1)
            {
                MessageBox.Show("Выберите хотя бы один лист!");
                return false;
            }

            IsAllColumnShouldBeEmpty = (bool)IsDeleteAllColumnEmpty_RadioButton.IsChecked;

            if (!IsAllColumnShouldBeEmpty && !int.TryParse(RowAfterColumnIsEmpty_Textbox.Text, out RowAfterColumnIsEmpty))
            {
                MessageBox.Show("Укажите правильный номер строки!");
                return false;
            };

            RowAfterColumnIsEmpty++;

            return true;
        }

        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var sheet in sheets.list) sheet.IsChecked = true;
            sheetsCollection.View.Refresh();
        }

        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var sheet in sheets.list) sheet.IsChecked = false;
            sheetsCollection.View.Refresh();
        }

        private void viewSourceSheet_Filter(object sender, FilterEventArgs e)
        {
            ExcelSheets.MySheet sheet = e.Item as ExcelSheets.MySheet;
            e.Accepted = sheet.Name.IndexOf(filterSheets.Text) >= 0;
        }

        private void filterSheets_TextChanged(object sender, TextChangedEventArgs e)
        {
            sheetsCollection.View.Refresh();
        }

        private void Number_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsNumber);
        }
        private void Number_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsNumber))
                e.CancelCommand();
        }

        bool IsNumber(char c)
        {
            if (c >= '0' && c <= '9')
                return true;
            return false;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
