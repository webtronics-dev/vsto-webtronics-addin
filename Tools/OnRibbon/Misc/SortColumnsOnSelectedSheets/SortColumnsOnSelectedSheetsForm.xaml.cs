﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;
using WebtronicsAddIn.Tools.Misc;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.SortColumnsOnSelectedSheets
{
    /// <summary>
    /// Логика взаимодействия для SortColumnsOnSelectedSheetsForm.xaml
    /// </summary>
    public partial class SortColumnsOnSelectedSheetsForm : Window
    {

        public CollectionViewSource sheetsCollection;
        public ExcelSheets.SheetsList sheets;
        private Excel.Application ExcelApp = Globals.ThisAddIn.Application;




        public Settings settings = new Settings();

        public class Settings
        {
            public CollectionViewSource AllColumnsCollection { get; set; }
            public ExcelClasses.Cells AllColumnCells { get; set; }

            public List<ExcelClasses.Cell> selectedColumns = new List<ExcelClasses.Cell>() ;
            public IEnumerable<ExcelSheets.MySheet> selectedSheets;

            public Excel.XlSortOrder sortOrder = Excel.XlSortOrder.xlAscending;
            public bool shouldMakeRowsBold;
            public int boldRowsQuantityPercent = 20;

            public int headersRow { get; set; }
        }

        

        public SortColumnsOnSelectedSheetsForm()
        {
            InitializeComponent();
            sheets = new ExcelSheets.SheetsList(ExcelSheets: ExcelFunctions.GetAllSheetsOfActiveWorkbook(), IsChecked: true);
            sheets.list[0].IsChecked = false;

            sheetsCollection = sheets.CreateCollectionViewSource();
            sheetsCollection.Filter += viewSourceSheet_Filter;
            sheetsGrid.ItemsSource = sheetsCollection.View;
        }

        private void getHeadersButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Office.Interop.Excel.Worksheet sheet;

            int shNumber = Int32.Parse(sheetNumberTextBox.Text);
            settings.headersRow = Int32.Parse(rowNumberTextBox.Text);

            sheet = ExcelFunctions.GetSheetByIndex(shNumber);
            if (sheet is null)
            {
                MessageBox.Show("Ошибка! Не найден указанный лист!");
                return;
            }

            settings.AllColumnCells  = new ExcelClasses.Cells(rng: ExcelFunctions.GetRowRange(sheet, settings.headersRow), IsChecked: false);
            if (settings.AllColumnCells.list is null || settings.AllColumnCells.list.Length < 1)
            {
                MessageBox.Show("Не найдено заголовков!");
                return;
            }

            settings.AllColumnsCollection = settings.AllColumnCells.createCollectionViewSource();
            settings.AllColumnsCollection.Filter += viewSourceColumns_Filter;
            columnsList.ItemsSource = settings.AllColumnsCollection.View;

        }

        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var sheet in sheets.list) sheet.IsChecked = true;
            sheetsCollection.View.Refresh();
        }

        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var sheet in sheets.list) sheet.IsChecked = false;
            sheetsCollection.View.Refresh();
        }

       private void viewSourceSheet_Filter(object sender, FilterEventArgs e)
        {
            ExcelSheets.MySheet sheet = e.Item as ExcelSheets.MySheet;
            e.Accepted = sheet.Name.IndexOf(filterSheets.Text) >= 0;
        }

        private void viewSourceColumns_Filter(object sender, FilterEventArgs e)
        {
            ExcelClasses.Cell cell = e.Item as ExcelClasses.Cell;
            e.Accepted = cell.stringValue.IndexOf(filterColumn.Text) >= 0;
        }

        private void filterSheets_TextChanged(object sender, TextChangedEventArgs e)
        {
            sheetsCollection.View.Refresh();
        }
        private void filterColumns_TextChanged(object sender, TextChangedEventArgs e)
        {
            settings.AllColumnsCollection.View.Refresh();
        }
        private void Number_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsNumber);
        }
        private void Number_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsNumber))
                e.CancelCommand();
        }

        bool IsNumber(char c)
        {
            if (c >= '0' && c <= '9')
                return true;
            return false;
        }

        private void RunButton_Click(object sender, RoutedEventArgs e)
        {
            bool isSettingsOk= getSettings();
            if (!isSettingsOk) return;

            var script = new SortColumnsOnSelectedSheetsLogic(settings);

            ExcelApp.ScreenUpdating = false;
            script.StartSorting();
            ExcelApp.ScreenUpdating = true;

            MessageBox.Show("Готово!");
            Close();

        }

        private bool getSettings()
        {
            settings.selectedColumns = new List<ExcelClasses.Cell>();
            if (settings.AllColumnsCollection is null) return false;
            var selectedSource = settings.AllColumnsCollection.Source as IEnumerable<ExcelClasses.Cell>;
            ExcelClasses.Cell column = selectedSource.FirstOrDefault(col => col.IsChecked == true);
            if (column is null)
            {
                MessageBox.Show("Выберите столбец для сортировки!");
                return false;
            }
            settings.selectedColumns.Add(column);


            settings.selectedSheets = from sheet in sheetsCollection.Source as IEnumerable<ExcelSheets.MySheet>
                                      where sheet.IsChecked
                                      select sheet;

            if (settings.selectedSheets.Count() < 1)
            {
                MessageBox.Show("Выберите хотя бы один лист для сортировки!");
                return false;
            }

            if (SortDataByCombobox.Text == "По убыванию (я-а)") settings.sortOrder = Excel.XlSortOrder.xlDescending;

            settings.shouldMakeRowsBold = (bool) shouldMakeRowBoldCheckbox.IsChecked;
            settings.boldRowsQuantityPercent = Convert.ToInt32(boldPercentageSlider.Value);

            return true;
        }
    }
}
