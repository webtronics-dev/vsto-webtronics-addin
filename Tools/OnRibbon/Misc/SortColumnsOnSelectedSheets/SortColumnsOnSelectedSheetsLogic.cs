﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebtronicsAddIn.Tools.Functions;
using WebtronicsAddIn.Tools.OnRibbon.CustomForms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.SortColumnsOnSelectedSheets
{
    class SortColumnsOnSelectedSheetsLogic
    {
        SortColumnsOnSelectedSheetsForm.Settings settings { get; set; }
        public SortColumnsOnSelectedSheetsLogic(SortColumnsOnSelectedSheetsForm.Settings inputSettings)
        {
            settings = inputSettings;
        }

        public void StartSorting()
        {
            foreach (ExcelSheets.MySheet CurrentSheet in settings.selectedSheets)
            {
                for (int i = 0; i < settings.selectedColumns.Count; i++)
                {
                    var ColumnCell = settings.selectedColumns[i];
                    int lastRow = ExcelFunctions.GetLastRowInSheet(CurrentSheet.SheetRef);
                    if (lastRow <= settings.headersRow) continue;

                    //Проверяем, есть ли значимые значение в нужном столбце
                    Excel.Range columnRange = ExcelFunctions.GetColumnRange(
                        sheet: CurrentSheet.SheetRef, columnNumber: ColumnCell.column,
                        firstRow: settings.headersRow + 1, lastRow: lastRow);

                    //Если нет значимых ячеек в диапазоне
                    if (!ExcelFunctions.IsSignificantValuesInRange(columnRange) && (i + 1) == settings.selectedColumns.Count && (i+i) < settings.AllColumnCells.list.Length)
                    {
                        CallEmptyColumnForm(ColumnCell, CurrentSheet); 
                        continue;
                    }

                    //Сортируем
                    ExcelFunctions.UnlinkListObjectsOnSheet(CurrentSheet.SheetRef);
                    ExcelFunctions.ClearAutofilter(CurrentSheet.SheetRef);
                    bool sorted = ExcelFunctions.SortColumnWithAutofilter(CurrentSheet.SheetRef, settings.sortOrder, ColumnCell.column, settings.headersRow);

                    if (!sorted) continue;

                    //Деламе жирным
                    if (settings.shouldMakeRowsBold) ApplyBoldRows(CurrentSheet.SheetRef, lastRow);

                    //Если отсортировали, нет необходимости дальше обрабатывать очередь
                    break;

                }
            }
        }



        private void ApplyBoldRows(Excel.Worksheet sheet, int lastRow)
        {
            double rowsWithData = lastRow - settings.headersRow;
            int rowsToBold = (int)Math.Round(rowsWithData / 100 * settings.boldRowsQuantityPercent);
            if (rowsToBold < 1) return;

            Excel.Range rng = ExcelFunctions.GetRowsRange(sheet, settings.headersRow + 1, rowsToBold);
            rng.Font.Bold = true;
        }

        private void CallEmptyColumnForm(ExcelClasses.Cell currentColumn, ExcelSheets.MySheet currentSheet)
        {
            settings.AllColumnCells.SetIsCheckedForAllCells(false);
            var form = new SelectFromDataGridCustomForm(settings.AllColumnsCollection, settings.selectedColumns);
            form.HeaderLabel.Content = "Выбранный столбец пуст!";
            form.HelpLabel.Text = $"В выбранном столбце ({currentColumn.ColumnLetter}-`{currentColumn.stringValue}` отсутствуют значения на листе `{currentSheet.Name}`" +
                $"Выберите другой столбец или нажмите 'Отмена' для пропуска листа";
            form.ShowDialog();
        }


    }
}
