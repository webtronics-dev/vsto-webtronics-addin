﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.XmlParser
{
    /// <summary>
    /// Логика взаимодействия для XmlParserForm.xaml
    /// </summary>
    public partial class XmlParserForm : Window
    {
        public XmlParserForm()
        {
            InitializeComponent();
        }

        private void runScript_Click(object sender, RoutedEventArgs e)
        {
            var dataSet = new DataSet();
            dataSet.ReadXml(PathToFile_TextBox.Text);
            var a = dataSet.CreateDataReader();
            //XMLDataGrid.ItemsSource = dataSet.Tables[0].DefaultView;
            XMLDataGrid.ItemsSource = a;
        }

        private void CloseButt_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ChooseFileOnPc_Button_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.Multiselect = false;
            openFileDlg.Filter = "Excel Files|*.xml;*.yml";

            Nullable<bool> result = openFileDlg.ShowDialog();

            if (result == true)
            {
                PathToFile_TextBox.Text = openFileDlg.FileName;
            }
        }
    }
}
