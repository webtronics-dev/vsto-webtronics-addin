﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.UrlToHyperlink
{
    public class UrlToHyperlinkMainLogic
    {
        public UrlToHyperlinkMainLogic(Settings _settings)
        {
            settings = _settings;
        }

        public Settings settings { get; set; }


        public class Settings
        {
            public List<Functions.ExcelSheets.MySheet> checkedSheets { get; set; }

            public bool isExcelHyperlinksMethod { get; set; }
        }


        public void startScript()
        {
            foreach (var sheetInfo in settings.checkedSheets)
            {
                Microsoft.Office.Interop.Excel.Worksheet sheet = Functions.ExcelFunctions.GetSheetByIndex(sheetInfo.Index);

                ExcelMethodHyperlinkInsertSheet(sheet, settings.isExcelHyperlinksMethod);

            }
        }

        void ExcelMethodHyperlinkInsertSheet(Microsoft.Office.Interop.Excel.Worksheet sheet, bool useExcelMethod)
        {
            var arrVals = Functions.ExcelFunctions.GetStringArrayFromRange(sheet.UsedRange);

            var rows = arrVals.GetLength(0);
            var columns = arrVals.GetLength(1);

            ushort urlsCount = 0;

            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {

                    if (urlsCount > 65529)
                    {
                        return;
                    }

                    string val = arrVals[row, column];
                    
                    if (!Functions.ExcelFunctions.IsItUrl(val)) continue;

                    if (useExcelMethod)
                    {
                        Functions.ExcelFunctions.InsertHyperlinkExcelMethod(sheet, row+1, column+1, val, "",val);
                        urlsCount++;
                    } else
                    {
                        Functions.ExcelFunctions.InsertHyperlinkFormulaMethod(sheet, row+1, column+1, val, val);
                    }

                }
            }
        }


    
    }
}
