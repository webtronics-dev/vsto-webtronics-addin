﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;

namespace WebtronicsAddIn.Tools.OnRibbon.Misc.UrlToHyperlink
{
    /// <summary>
    /// Логика взаимодействия для UrlToHyperlinkForm.xaml
    /// </summary>
    public partial class UrlToHyperlinkForm : Window
    {

        public List<ExcelSheets.MySheet> sheets { get; set; }

        public UrlToHyperlinkForm(Dictionary<int, string> dictionarySheets)
        {
            InitializeComponent();
            AddSheetsDictionaryToListView(dictionarySheets);
        }

        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var sheetList in sheets)
            {
                sheetList.IsChecked = true;
            }
            sheetsGrid.ItemsSource = null;
            sheetsGrid.ItemsSource = sheets;
        }

        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var sheetList in sheets)
            {
                sheetList.IsChecked = false;
            }

            sheetsGrid.ItemsSource = null;
            sheetsGrid.ItemsSource = sheets;
        }

        private void AddSheetsDictionaryToListView(Dictionary<int, string> sheetsDic)
        {
           sheets = new List<ExcelSheets.MySheet>();

            foreach (KeyValuePair<int, string> sheet in sheetsDic)
            {
                sheets.Add(new ExcelSheets.MySheet() { IsChecked = false, Index = sheet.Key, Name = sheet.Value }); ;
            }

            //SheetsListView.ItemsSource = sheets;
            sheetsGrid.ItemsSource = sheets;
        }

        private void UrlToHyperlinkMainButton_Click(object sender, RoutedEventArgs e)
        {
            var settings = GetSettings();
            if (settings.checkedSheets.Count < 1)
            {
                MessageBox.Show("Выберите хотя бы один лист!", "Ошибка!");
                return;
            }

            var script = new UrlToHyperlinkMainLogic(settings);

            script.startScript();

            MessageBox.Show("Ок!");
        }


        public UrlToHyperlinkMainLogic.Settings GetSettings()
        {

            var checkedSheets = from sh in sheets
                                where sh.IsChecked == true
                                select sh;

            var settings = new UrlToHyperlinkMainLogic.Settings
            {
                isExcelHyperlinksMethod = (bool)excelMethod.IsChecked,
                checkedSheets = checkedSheets.ToList()
            };

            return settings;

        }

        private void CloseButt_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }


}
