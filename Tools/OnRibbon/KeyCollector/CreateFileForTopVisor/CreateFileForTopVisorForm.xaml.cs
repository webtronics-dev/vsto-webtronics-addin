﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;
using static WebtronicsAddIn.Tools.Functions.ExcelClasses;
using Excel = Microsoft.Office.Interop.Excel;
namespace WebtronicsAddIn.Tools.OnRibbon.KeyCollector.CreateFileForTopVisor
{
    /// <summary>
    /// Логика взаимодействия для CreateFileForTopVisorForm.xaml
    /// </summary>
    public partial class CreateFileForTopVisorForm : Window
    {
        private Excel.Worksheet mapSheet;
        private CollectionViewSource viewSourceFrequency;
        private CollectionViewSource viewSourceUrlColumn;
        private int selectedColumn;
        private int urlColumnNum;
        private int headersRow;
        private string SavePath = "";
        private Excel.Application ExcelApp = Globals.ThisAddIn.Application;

        public CreateFileForTopVisorForm()
        {
            InitializeComponent();
            mapSheet = KeyCollectorFunctions.getMapOrContentsSheet();
        }

        private void getHeadersButton_Click(object sender, RoutedEventArgs e)
        {
            Excel.Worksheet sheet;

            int shNumber = int.Parse(sheetNumberTextBox.Text);
            headersRow = int.Parse(rowNumberTextBox.Text);

            sheet = ExcelFunctions.GetSheetByIndex(shNumber);
            if (sheet is null)
            {
                MessageBox.Show("Ошибка! Не найден указанный лист!");
                return;
            }

            var headersFrequency = new ExcelClasses.Cells(rng: ExcelFunctions.GetRowRange(sheet, headersRow), IsChecked: false);
            if (headersFrequency.list is null || headersFrequency.list.Length < 1)
            {
                MessageBox.Show("Не найдено заголовков!");
                return;
            }

            viewSourceFrequency = headersFrequency.createCollectionViewSource();
            viewSourceFrequency.Filter += viewSource_Filter;
            headersGridSummFrequency.ItemsSource = viewSourceFrequency.View;

            var headersUrl = new ExcelClasses.Cells(rng: ExcelFunctions.GetRowRange(sheet, headersRow), IsChecked: false);
            viewSourceUrlColumn = headersUrl.createCollectionViewSource();
            viewSourceUrlColumn.Filter += viewSource_Filter;
            headersGridUrlColumn.ItemsSource = viewSourceUrlColumn.View;
        }


        void viewSource_Filter(object sender, FilterEventArgs e)
        {

            Cell myValue = e.Item as Cell;

            e.Accepted = (myValue.stringValue).IndexOf(filter.Text) >= 0;
        }
        private void filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            viewSourceFrequency.View.Refresh();
        }

        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {

            foreach (Cell item in headersGridSummFrequency.Items)
            {
                item.IsChecked = true;
            }

            viewSourceFrequency.View.Refresh();

        }
        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (Cell item in headersGridSummFrequency.Items)
            {
                item.IsChecked = false;
            }

            viewSourceFrequency.View.Refresh();
        }

        private void Number_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsNumber);
        }
        private void Number_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsNumber))
                e.CancelCommand();
        }

        bool IsNumber(char c)
        {
            if (c >= '0' && c <= '9')
                return true;
            return false;
        }
        bool IsLetter(char c)
        {
            if (c >= 'a' && c <= 'z')
                return true;
            if (c >= 'A' && c <= 'Z')
                return true;
            return false;
        }

        private void ColumnName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsLetter);
        }
        private void ColumnName_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsLetter))
                e.CancelCommand();
        }

        private void CloseButt_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void runScript_Click(object sender, RoutedEventArgs e)
        {
            if (!isValidForm()) return;

            bool IsCreateXLSX = (bool)IsCreateXLSX_Checkbox.IsChecked;

            var cellsWithLinks = GetCellsWithLinks();

            //Получаем все данные со всех листов
            var ListOfSheetData = new List<dynamic[,]>();
            int RequestsCount = 0;
            foreach (CellWithLinkToSheet cellWithLink in cellsWithLinks.list)
            {
                if (cellWithLink.SheetName == "Справка" || cellWithLink.SheetName == "Карта" || cellWithLink.SheetName == "Оглавление") continue;
                var data = ExcelFunctions.GetSheetValues(ExcelFunctions.GetSheetByName(cellWithLink.SheetName), headersRow + 1, 1);
                ListOfSheetData.Add(data);
                RequestsCount += data.GetLength(0);
            }

            var result = GetArrayFromDataList(ListOfSheetData, RequestsCount);

            if (IsCreateXLSX)
            {
                var wb = ExcelFunctions.AddNewWorkbook();
                var sh = ExcelFunctions.GetSheetByIndex(1);
                sh.Range["A1:D" + RequestsCount].Value = result;
                wb.SaveAs(SavePath.Replace(".txt", ".xlsx"));
                wb.Close();
            }


            string[] resultTXT = new string[result.GetLength(0)];
            for (int i = 0; i < result.GetLength(0); i++)
            {
                string str = "";
                for (int j = 0; j <= 2; j++) str += result[i, j] + ";";
                resultTXT[i] = str;
            }

            System.IO.File.WriteAllLines(SavePath, resultTXT);

            MessageBox.Show("Готово!");
            this.Close();
        }

        private CellsWithLinkToSheet GetCellsWithLinks()
        {
            //получаем все ссылки на листы
            CellsWithLinkToSheet cellsWithLinks = new CellsWithLinkToSheet(mapSheet.Hyperlinks);
            if (cellsWithLinks.list.Count < 1) cellsWithLinks = new CellsWithLinkToSheet(mapSheet.UsedRange, isKeyCollectorFormula: true);
            return cellsWithLinks;
        }
        private dynamic[,] GetArrayFromDataList(List<dynamic[,]> ListOfSheetData, int RequestsCount)
        {
            var output_arr = new dynamic[RequestsCount, 4];
            int last_row = 0;
            //пишем нужные данные в новый массив
            foreach (dynamic[,] data in ListOfSheetData)
            {
                //ищем индекс максимального значения
                var columnArray = Enumerable.Range(1, data.GetLength(0))
                        .Select(x => data[x, selectedColumn])
                        .ToArray();

                var maxValueIndex = Array.IndexOf(columnArray, columnArray.Max());

                //получаем имя группы
                var group = data[maxValueIndex + 1, 1];

                for (int i = 1; i <= data.GetLength(0); i++)
                {
                    output_arr[last_row, 0] = data[i, 1];
                    output_arr[last_row, 1] = group;
                    output_arr[last_row, 3] = ";";
                    if (urlColumnNum > 0) output_arr[last_row, 2] = data[i, urlColumnNum];
                    last_row++;
                }
            }
            return output_arr;
        }

        private bool isValidForm()
        {
            if (SavePath == "" || !getSelectedColumns())
            {
                MessageBox.Show("Заполните необходимые поля!");
                return false;
            }
            return true;
        }


        private bool getSelectedColumns()
        {
            if (viewSourceFrequency is null)
            {
                MessageBox.Show("Выберите столбец для поиска максимальной частотности и выбора группы!", "Ошибка!");
                return false;
            }

            var selectedColumns = from header in viewSourceFrequency.Source as IEnumerable<Cell>
                              where header.IsChecked
                              select header.column;

            if (selectedColumns.Count() < 1)
            {
                MessageBox.Show("Выберите столбец поиска максимальной частотности и выбора группы!", "Ошибка!");
                return false;
            }

            selectedColumn = selectedColumns.ElementAt(0);

            var urlColumns = viewSourceUrlColumn.Source as IEnumerable<Cell>;
            urlColumnNum = urlColumns.FirstOrDefault(col => col.IsChecked == true)?.column ?? 0;

            return true;
        }

        private void ChooseSavePath_Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = ExcelFunctions.GetActiveWorkbook().Name + "_topvisor"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                SavePath = dlg.FileName;
                SavePath_Textbox.Text = dlg.SafeFileName;
            }
        }
    }
}
