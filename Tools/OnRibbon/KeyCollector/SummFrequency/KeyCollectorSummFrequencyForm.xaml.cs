﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using static WebtronicsAddIn.Tools.Functions.ExcelClasses;

namespace WebtronicsAddIn.Tools.OnRibbon.KeyCollector.SummFrequency
{
    /// <summary>
    /// Логика взаимодействия для KeyCollectorSummFrequencyForm.xaml
    /// </summary>
    public partial class KeyCollectorSummFrequencyForm : Window
    {

        private Excel.Worksheet mapSheet;
        private CollectionViewSource viewSourceSummFrequency;
        private CollectionViewSource viewSourceUrlColumn;
        private IEnumerable<string> selectedColumns;
        private IEnumerable<string> selectedColumnsNames;

        private int urlColumnNum;
        private int headersRow;
        private int outputFirstColumnNumber;
        private int frequencyDivider = 0;
        private bool isConvertTextNumberToNumber;
        private string mapSheetName;
        private Excel.Application ExcelApp = Globals.ThisAddIn.Application;

        //public string tooltip_DivideFrequency = "Позволяет высчитать частотность за квартал(разделить на 4) или за месяц(разделить на 12), если собраны данные за год";

        public KeyCollectorSummFrequencyForm()
        {
            InitializeComponent();
            mapSheet = KeyCollectorFunctions.getMapOrContentsSheet();
        }

        private void getFormSettings()
        {
            if ((bool)radioPasteToCustomColumn.IsChecked) outputFirstColumnNumber = ExcelFunctions.NumberFromExcelColumn(whereToPaste.Text);
            if ((bool)pasteToLastColumn.IsChecked) outputFirstColumnNumber = ExcelFunctions.GetLastColumnInSheet(mapSheet) + 1;
            if ((bool)checkbox_DivideFrequency.IsChecked) frequencyDivider = int.Parse(frequencyDividerTextBox.Text);
            isConvertTextNumberToNumber = (bool)ConvertTextToNumber_Checkbox.IsChecked;
            mapSheetName = ExcelFunctions.GetActiveWorksheet().Name;
        }

        private bool getSelectedColumns()
        {
            if (viewSourceSummFrequency is null)
            {
                MessageBox.Show("Выберите хотя бы один столбец для суммирования частотностей!", "Ошибка!");
                return false;
            }

            selectedColumns = from header in viewSourceSummFrequency.Source as IEnumerable<Cell>
                              where header.IsChecked
                              select header.ColumnLetter;

            selectedColumnsNames = from header in viewSourceSummFrequency.Source as IEnumerable<Cell>
                                   where header.IsChecked
                                   select header.stringValue;

            if (selectedColumns.Count() < 1)
            {
                MessageBox.Show("Выберите хотя бы один столбец для суммирования частотностей!", "Ошибка!");
                return false;
            }

            var urlColumns = viewSourceUrlColumn.Source as IEnumerable<Cell>;
            urlColumnNum = urlColumns.FirstOrDefault(col => col.IsChecked == true)?.column ?? 0;

            return true;
        }
        private void runScript_Click(object sender, RoutedEventArgs e)
        {

            if (!KeyCollectorFunctions.isMapOrContentsSheetsExist() || !getSelectedColumns()) return;

            runScriptButton.IsEnabled = false;
            WaitTextBlock.Visibility = Visibility.Visible;

            getFormSettings();

            ExcelFunctions.UnmergeCellsInSheet(mapSheet);

            ExcelApp.ScreenUpdating = false;
            ExcelApp.Calculation = Excel.XlCalculation.xlCalculationManual;

            summFrequency();

            writeHeaders();
            cleanFormats();
            ExcelApp.ScreenUpdating = true;
            ExcelApp.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
            WaitTextBlock.Visibility = Visibility.Hidden;
            MessageBox.Show("Готово!", "Готово!");
            Close();
        }

        private void summFrequency()
        {
            bool isSheetWithFormulaHyperlink = false;

            CellsWithLinkToSheet cellsWithLinks = new CellsWithLinkToSheet(mapSheet.Hyperlinks);
            if (cellsWithLinks.list.Count < 1)
            {
                cellsWithLinks = new CellsWithLinkToSheet(mapSheet.UsedRange, isKeyCollectorFormula:true);
                isSheetWithFormulaHyperlink = true;
            }

            foreach (CellWithLinkToSheet cellWithLink in cellsWithLinks.list)
            {
                writeCountOfRequestsFormulaOnSheet(cellWithLink.row, cellWithLink.SheetName);
                writeFrequencyFormulaOnSheet(cellWithLink.row, cellWithLink.SheetName);
                if (urlColumnNum > 0) writeURLFormulaArrayOnSheet(cellWithLink.row, cellWithLink.SheetName);
                if (isSheetWithFormulaHyperlink)
                {
                    string oldCellValue = Convert.ToString(mapSheet.Range[cellWithLink.getCellAddress()].Value);
                    mapSheet.Range[cellWithLink.getCellAddress()].Value = "";
                    ExcelFunctions.InsertHyperlinkExcelMethod(mapSheet, cellWithLink.row, cellWithLink.column, "", $"'{cellWithLink.SheetName}'!A1", oldCellValue);
                }
                if (isConvertTextNumberToNumber) {
                    var sheet = ExcelFunctions.GetSheetByName(cellWithLink.SheetName);
                    ExcelFunctions.ConvertTextNumberToRealNumber(sheet);
                    ExcelFunctions.CreateLinkToContentsSheet(sheet, mapSheetName);
                }

            }
        }

        private void cleanFormats()
        {
            ExcelFunctions.GetColumnRange(mapSheet, outputFirstColumnNumber).ClearFormats();
            ExcelFunctions.GetColumnRange(mapSheet, outputFirstColumnNumber + 1).ClearFormats();
            ExcelFunctions.GetColumnRange(mapSheet, outputFirstColumnNumber + 2).ClearFormats();
        }
        private void writeHeaders()
        {
            if (!KeyCollectorFunctions.isKeyCollectorHeadersExist(mapSheet))
            {
                Excel.Range row = (Excel.Range)mapSheet.Rows[1];
                row.Insert();
            }
            string quantityOfRequestsColumn = ExcelFunctions.ExcelColumnFromNumber(outputFirstColumnNumber);
            string selectedFrequences = string.Join(";", selectedColumnsNames);
            string[,] headers;


            headers = new string[,] { { "Количество запросов", "URL", $"Суммарная частотность ({selectedFrequences})" } };


            mapSheet.Range[mapSheet.Cells[1, outputFirstColumnNumber], mapSheet.Cells[1, outputFirstColumnNumber + 2]].Value = headers;
            mapSheet.Range[mapSheet.Cells[1, outputFirstColumnNumber - 1], mapSheet.Cells[1, outputFirstColumnNumber - 1]].Formula = $"=\"Всего запросов: \" & SUM({quantityOfRequestsColumn}:{quantityOfRequestsColumn})";
        }

        private void writeCountOfRequestsFormulaOnSheet(int row, string sheetName)
        {
            string formula = createCountOfRequestsFormula(sheetName);
            Excel.Range cell = (Excel.Range)mapSheet.Cells[row, outputFirstColumnNumber];
            cell.Formula = formula;
        }
        private void writeURLFormulaArrayOnSheet(int row, string sheetName)
        {
            var sheet = ExcelFunctions.GetSheetByName(sheetName);
            if (sheet is null) return;
            string lastValue = ExcelFunctions.GetLastValueInColumn(sheet, urlColumnNum);
            if (lastValue is null || !ExcelFunctions.IsItUrl(lastValue)) return;
            Excel.Range cell = (Excel.Range)mapSheet.Cells[row, outputFirstColumnNumber + 1];
            cell.Value = lastValue;
        }
        private void writeFrequencyFormulaOnSheet(int row, string sheetName)
        {
            string formula = createSummFrequencyFormula(sheetName, selectedColumns);
            Excel.Range cell = (Excel.Range)mapSheet.Cells[row, outputFirstColumnNumber + 2];
            cell.Formula = formula;
        }
        private string createCountOfRequestsFormula(string sheetName)
        {
            return $"=CountA('{sheetName}'!A:A)-{headersRow}";
        }
        private string createSummFrequencyFormula(string sheetName, IEnumerable<string> columns)
        {
            string formula = "";
            foreach (var column in columns)
            {
                string columnFormula = $"SUM('{sheetName}'!{column}:{column})";
                formula = (formula.Length < 1) ? columnFormula : $"{formula}+{columnFormula}";
            }

            if (frequencyDivider > 0) formula = $"ROUNDDOWN(({formula})/{frequencyDivider}, 0)";

            return $"={formula}";
        }
        private void getHeadersButton_Click(object sender, RoutedEventArgs e)
        {
            Excel.Worksheet sheet;

            int shNumber = int.Parse(sheetNumberTextBox.Text);
            headersRow = int.Parse(rowNumberTextBox.Text);

            sheet = ExcelFunctions.GetSheetByIndex(shNumber);
            if (sheet is null)
            {
                MessageBox.Show("Ошибка! Не найден указанный лист!");
                return;
            }

            var headersFrequency = new ExcelClasses.Cells(rng: ExcelFunctions.GetRowRange(sheet, headersRow), IsChecked: false);
            if (headersFrequency.list is null || headersFrequency.list.Length < 1)
            {
                MessageBox.Show("Не найдено заголовков!");
                return;
            }

            viewSourceSummFrequency = headersFrequency.createCollectionViewSource();
            viewSourceSummFrequency.Filter += viewSource_Filter;
            headersGridSummFrequency.ItemsSource = viewSourceSummFrequency.View;

            var headersUrl = new ExcelClasses.Cells(rng: ExcelFunctions.GetRowRange(sheet, headersRow), IsChecked: false);
            viewSourceUrlColumn = headersUrl.createCollectionViewSource();
            viewSourceUrlColumn.Filter += viewSource_Filter;
            headersGridUrlColumn.ItemsSource = viewSourceUrlColumn.View;

        }

        void viewSource_Filter(object sender, FilterEventArgs e)
        {

            Cell myValue = e.Item as Cell;

            e.Accepted = (myValue.stringValue).IndexOf(filter.Text) >= 0;
        }
        private void filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            viewSourceSummFrequency.View.Refresh();
        }

        private void selectAll_Checked(object sender, RoutedEventArgs e)
        {

            foreach (Cell item in headersGridSummFrequency.Items)
            {
                item.IsChecked = true;
            }

            viewSourceSummFrequency.View.Refresh();

        }
        private void selectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (Cell item in headersGridSummFrequency.Items)
            {
                item.IsChecked = false;
            }

            viewSourceSummFrequency.View.Refresh();
        }

        private void Number_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsNumber);
        }
        private void Number_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsNumber))
                e.CancelCommand();
        }

        bool IsNumber(char c)
        {
            if (c >= '0' && c <= '9')
                return true;
            return false;
        }
        bool IsLetter(char c)
        {
            if (c >= 'a' && c <= 'z')
                return true;
            if (c >= 'A' && c <= 'Z')
                return true;
            return false;
        }

        private void ColumnName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !e.Text.All(IsLetter);
        }
        private void ColumnName_OnPasting(object sender, DataObjectPastingEventArgs e)
        {
            var stringData = (string)e.DataObject.GetData(typeof(string));
            if (stringData == null || !stringData.All(IsLetter))
                e.CancelCommand();
        }

        private void CloseButt_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
