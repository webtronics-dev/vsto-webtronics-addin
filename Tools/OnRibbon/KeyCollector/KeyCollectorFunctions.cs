﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.OnRibbon.KeyCollector
{
    static class KeyCollectorFunctions
    {
        public static bool isMapOrContentsSheetsExist()
        {
            if (ExcelFunctions.GetSheetByName("Карта") is null && ExcelFunctions.GetSheetByName("Оглавление") is null)
            {
                MessageBox.Show("Не найден лист 'Карта' или 'Оглавление'!", "Ошибка!");
                return false;
            }

            return true;
        }

        public static bool isKeyCollectorHeadersExist(Excel.Worksheet sheet)
        {
            var headersRow = ExcelFunctions.GetRowValues(sheet, 1);

            foreach (string cell in headersRow)
            {
                if (cell != null && cell.Contains("Количество запросов")) return true;
            }
            return false;
        }

        public static int getSummFrequencyColumnNumber(Excel.Worksheet sheet)
        {
            var headersRow = ExcelFunctions.GetRowValues(sheet, 1);
            for (int i = 0; i < headersRow.GetLength(1); i++)
            {
                if (headersRow[0,i] != null && headersRow[0, i].Contains("Суммарная частотность (")) return (i +1);
            }
            return 0;
        }

        public static int getRequestsQuantityColumnNumber(Excel.Worksheet sheet)
        {
            var headersRow = ExcelFunctions.GetRowValues(sheet, 1);
            for (int i = 0; i < headersRow.GetLength(1); i++)
            {
                if (headersRow[0, i] != null && headersRow[0, i].Contains("Количество запросов")) return (i + 1);
            }
            return 0;
        }

        public static Excel.Worksheet getMapOrContentsSheet()
        {
            Excel.Worksheet mapSheet = ExcelFunctions.GetSheetByName("Карта");
            if (mapSheet is null) mapSheet = ExcelFunctions.GetSheetByName("Оглавление");
            return mapSheet;
        }


    }
}
