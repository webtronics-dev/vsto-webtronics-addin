﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebtronicsAddIn.Tools.Functions;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.OnRibbon.KeyCollector.TransformSheetForGSheets
{
    /// <summary>
    /// Логика взаимодействия для TransformSheetForGSheetsForm.xaml
    /// </summary>
    public partial class TransformSheetForGSheetsForm : Window
    {
        private Excel.Worksheet mapSheet;
        private int requestsColumnNumber;
        private string[,] headersArray;

        private bool shouldApplyConditionalFormatting;
        private bool shouldGroupData;

        private Dictionary<string, ExcelClasses.Column> columnsOrder;
        public TransformSheetForGSheetsForm()
        {
            InitializeComponent();
        }

        private void CloseButt_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void runButton_Click(object sender, RoutedEventArgs e)
        {
            bool settings = getSettings();
            if (!settings) return;


            ExcelFunctions.AddColumnsAfter(mapSheet, columnsOrder["URL"].number, 5);

            writeFormulas();
            writeHeaders();

            if (shouldApplyConditionalFormatting) applyAllConditionalFormatting();
            if (shouldGroupData) groupData();
            MessageBox.Show("Готово!");
            Close();

        }

        private void writeHeaders()
        {
            Excel.Range rng = mapSheet.Range[mapSheet.Cells[1, requestsColumnNumber], mapSheet.Cells[1, requestsColumnNumber + headersArray.GetLength(1) - 1]];
            rng.Value = headersArray;
            rng.Columns.AutoFit();
        }
        private void writeFormulas()
        {
            Excel.Range row = (Excel.Range)mapSheet.Rows[1];
            row.EntireRow.Insert(CopyOrigin: Excel.XlInsertFormatOrigin.xlFormatFromLeftOrAbove);
            ExcelFunctions.GetCellAsRange(mapSheet, 2, 1).EntireRow.Value = "";

            var lastRow = ExcelFunctions.GetLastRowInSheet(mapSheet);

            ExcelFunctions.GetCellAsRange(mapSheet, 2, columnsOrder["trafficMin"].number)
                .Formula = $"=SUM({columnsOrder["trafficMin"].letter}3:{columnsOrder["trafficMin"].letter}{lastRow})";
            ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["trafficMin"].number, 3, lastRow)
                .Formula = $"=Floor(Floor({columnsOrder["summFrequency"].letter}3 * 0.03, 1) * 1.5, 1)";

            ExcelFunctions.GetCellAsRange(mapSheet, 2, columnsOrder["trafficMax"].number)
                .Formula = $"=SUM({columnsOrder["trafficMax"].letter}3:{columnsOrder["trafficMax"].letter}{lastRow})";
            ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["trafficMax"].number, 3, lastRow)
                .Formula = $"=Floor(Floor({columnsOrder["summFrequency"].letter}3 * 0.25, 1) * 1.5, 1)";


            ExcelFunctions.GetCellAsRange(mapSheet, 2, columnsOrder["conversionMin"].number)
                .Formula = $"=SUM({columnsOrder["conversionMin"].letter}3:{columnsOrder["conversionMin"].letter}{lastRow})";
            ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["conversionMin"].number, 3, lastRow)
                .Formula = $"=Floor({columnsOrder["trafficMin"].letter}3 * 0.01, 1)";


            ExcelFunctions.GetCellAsRange(mapSheet, 2, columnsOrder["conversionMax"].number)
                .Formula = $"=SUM({columnsOrder["conversionMax"].letter}3:{columnsOrder["conversionMax"].letter}{lastRow})";
            ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["conversionMax"].number, 3, lastRow)
                .Formula = $"=Floor({columnsOrder["trafficMax"].letter}3 * 0.01, 1)";

            ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["titleLength"].number, 3, lastRow)
                .Formula = $"=LEN({columnsOrder["Title"].letter}3)";

            ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["H1Length"].number, 3, lastRow)
                .Formula = $"=LEN({columnsOrder["H1"].letter}3)";

            ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["descriptionLength"].number, 3, lastRow)
                .Formula = $"=LEN({columnsOrder["Description"].letter}3)";

        }

        private void groupData()
        {
            mapSheet.UsedRange.ClearOutline();
            int firstCol = mapSheet.Hyperlinks[1].Range.Column - 1;

            foreach (Excel.Hyperlink link in mapSheet.Hyperlinks)
            {
                link.Range.EntireRow.OutlineLevel = (link.Range.Column > firstCol) ? link.Range.Column - firstCol : link.Range.Column;
            }
        }
        private void applyAllConditionalFormatting()
        {
            applyConditionalFormatting(ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["summFrequency"].number, 2));
            applyConditionalFormatting(ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["trafficMin"].number, 2));
            applyConditionalFormatting(ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["trafficMax"].number, 2));
            applyConditionalFormatting(ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["conversionMin"].number, 2));
            applyConditionalFormatting(ExcelFunctions.GetColumnRange(mapSheet, columnsOrder["conversionMax"].number, 2));
        }
        private void applyConditionalFormatting(Excel.Range diap)
        {
            Excel.ColorScale colorScale = (Excel.ColorScale)diap.FormatConditions.AddColorScale(ColorScaleType: 3);
            colorScale.SetFirstPriority();

            colorScale.ColorScaleCriteria[1].Type = Excel.XlConditionValueTypes.xlConditionValueLowestValue;
            colorScale.ColorScaleCriteria[1].FormatColor.Color = 8109667;

            colorScale.ColorScaleCriteria[2].Type = Excel.XlConditionValueTypes.xlConditionValuePercentile;
            colorScale.ColorScaleCriteria[2].Value = 50;
            colorScale.ColorScaleCriteria[2].FormatColor.Color = 8711167;

            colorScale.ColorScaleCriteria[3].Type = Excel.XlConditionValueTypes.xlConditionValueHighestValue;
            colorScale.ColorScaleCriteria[3].FormatColor.Color = 7039480;
        }

        private string[,] getHeadersArray()
        {
            return new string[,] { {
                    "Количество запросов",
                    "URL",
                    "Title",
                    "H1",
                    "Description",
                    "Keywords",
                    "Готовность",
                    "Ссылка на гуглдок",
                    "Потенциал трафика",
                    "Прогноз трафика в месяц если запросы на 10 месте(МИНИМУМ)",
                    "Прогноз трафика в месяц если запросы на 1 месте(МАКСИМУМ)",
                    "Прогноз конверсии в регистрацию в месяц(из расчета 1 %) если запросы на 10 месте(МИНИМУМ)",
                    "Прогноз конверсии в регистрацию в месяц(из расчета 1 %) если запросы на 1 месте(МАКСИМУМ)",
                    "Кол-во символов TITLE",
                    "Кол-во символов H1",
                    "Кол-во символов Description",
                } };
        }
        private bool getSettings()
        {
            mapSheet = KeyCollectorFunctions.getMapOrContentsSheet();

            if (mapSheet is null)
            {
                MessageBox.Show("Не найден лист 'Карта' или 'Оглавление' - похоже, вы пытаетесь обработать файл не из Key Collector!");
                return false;
            }

            if (!KeyCollectorFunctions.isKeyCollectorHeadersExist(mapSheet))
            {
                MessageBox.Show("Не найдены необходимые заголовки! Перед запуском инструмента необходимо просуммировать частотности");
                return false;
            }

            requestsColumnNumber = KeyCollectorFunctions.getRequestsQuantityColumnNumber(mapSheet);

            if (requestsColumnNumber < 1)
            {
                MessageBox.Show("Не найден столбец с количеством запросов! Перед запуском инструмента необходимо просуммировать частотности");
                return false;
            }

            headersArray = getHeadersArray();
            columnsOrder = getColumnsOrder();

            shouldApplyConditionalFormatting = (bool)checkbox_shouldApplyConditionalFormatting.IsChecked;
            shouldGroupData = (bool)checkbox_shouldGroupData.IsChecked;

            return true;
        }
        private Dictionary<string, ExcelClasses.Column> getColumnsOrder()
        {
            Dictionary<string, ExcelClasses.Column> columns = new Dictionary<string, ExcelClasses.Column>();

            columns.Add("quantityOfRequests", new ExcelClasses.Column { header = "Количество запросов", number = requestsColumnNumber });
            columns.Add("URL", new ExcelClasses.Column { header = "URL", number = requestsColumnNumber + 1 });
            columns.Add("Title", new ExcelClasses.Column { header = "Title", number = requestsColumnNumber + 2 });
            columns.Add("H1", new ExcelClasses.Column { header = "H1", number = requestsColumnNumber + 3 });
            columns.Add("Description", new ExcelClasses.Column { header = "Description", number = requestsColumnNumber + 4 });
            columns.Add("Keywords", new ExcelClasses.Column { header = "Keywords", number = requestsColumnNumber + 5 });
            columns.Add("Готовность", new ExcelClasses.Column { header = "Готовность", number = requestsColumnNumber + 6 });
            columns.Add("Ссылка на гуглдок", new ExcelClasses.Column { header = "Ссылка на гуглдок", number = requestsColumnNumber + 7 });
            columns.Add("summFrequency", new ExcelClasses.Column { header = "Потенциал трафика", number = requestsColumnNumber + 8 });

            columns.Add("trafficMin", new ExcelClasses.Column { header = "Прогноз трафика в месяц если запросы на 10 месте(МИНИМУМ)", number = requestsColumnNumber + 9 });
            columns.Add("trafficMax", new ExcelClasses.Column { header = "Прогноз трафика в месяц если запросы на 1 месте(МАКСИМУМ)", number = requestsColumnNumber + 10 });

            columns.Add("conversionMin", new ExcelClasses.Column { header = "Прогноз конверсии в регистрацию в месяц(из расчета 1 %) если запросы на 10 месте(МИНИМУМ)", number = requestsColumnNumber + 11 });
            columns.Add("conversionMax", new ExcelClasses.Column { header = "Прогноз конверсии в регистрацию в месяц(из расчета 1 %) если запросы на 1 месте(МАКСИМУМ)", number = requestsColumnNumber + 12 });

            columns.Add("titleLength", new ExcelClasses.Column { header = "Кол-во символов TITLE", number = requestsColumnNumber + 13 });
            columns.Add("H1Length", new ExcelClasses.Column { header = "Кол-во символов H1", number = requestsColumnNumber + 14 });
            columns.Add("descriptionLength", new ExcelClasses.Column { header = "Кол-во символов Description", number = requestsColumnNumber + 15 });

            return columns;
        }
    }
}
