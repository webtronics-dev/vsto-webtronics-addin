﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Excel = Microsoft.Office.Interop.Excel;
namespace WebtronicsAddIn.Tools.Functions
{
    public class ExcelSheets
    {
        public class MySheet
        {
            public bool IsChecked { get; set; }

            public int Index { get; set; }

            public string Name { get; set; }

            public Excel.Worksheet SheetRef { get; set; }

        }

        public class SheetsList 
        {
            public MySheet[] list { get; set; }

            public SheetsList(Excel.Sheets ExcelSheets, bool IsChecked = false)
            {
                list = new MySheet[ExcelSheets.Count];

                foreach (Excel.Worksheet excelSheet in ExcelSheets)
                {
                    list[excelSheet.Index - 1] = new MySheet() { Name = excelSheet.Name, Index = excelSheet.Index, IsChecked = IsChecked, SheetRef = excelSheet };
                }
            }


            public ObservableCollection<MySheet> CreateObservableCollection()
            {
                ObservableCollection<MySheet> collection = new ObservableCollection<MySheet>();
                foreach (MySheet sheet in this.list) collection.Add(sheet);

                return collection;
            }

            public CollectionViewSource CreateCollectionViewSource()
            {
                var observableColl = this.CreateObservableCollection();
                var viewSource = new CollectionViewSource();

                viewSource.Source = observableColl;
                return viewSource;
            }

        }

    }
}