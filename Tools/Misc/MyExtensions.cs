﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebtronicsAddIn.Tools.Misc
{
    public static class MyExtensions
    {
            public static bool IsStringNumeric(this string text) => double.TryParse(text, out _);

        public static bool IsObjectNumber(this object value)
        {
            return value is sbyte
                    || value is byte
                    || value is short
                    || value is ushort
                    || value is int
                    || value is uint
                    || value is long
                    || value is ulong
                    || value is float
                    || value is double
                    || value is decimal;
        }

        /// <summary>
        /// Проверяет значимость динамичесткого объекта (если длина строки > 0 или число > 0)
        /// для массивов и остального возвращает null
        /// </summary>
        public static bool? IsDynamicSignificant(dynamic val)
        {
            if (val is string) return val.Length > 0;
            if (MyExtensions.IsObjectNumber(val)) return val > 0;
            return null;
        }

    }
}
