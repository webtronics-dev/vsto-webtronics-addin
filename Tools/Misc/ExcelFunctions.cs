﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows;
using WebtronicsAddIn.Tools.Misc;

namespace WebtronicsAddIn.Tools.Functions
{
    static class ExcelFunctions
    {
        public static int ConvertMediaColorToOle(System.Windows.Media.Color color)
        {
            var drawingcolor = System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B);
            return System.Drawing.ColorTranslator.ToOle(drawingcolor);
        }
        public static void ConvertTextNumberToRealNumber(Excel.Worksheet sheet)
        {
            var rng = sheet.UsedRange;
            rng.NumberFormat = "General";
            rng.Value = rng.Value;
        }

        public static void CreateLinkToContentsSheet(Excel.Worksheet sheet, string contentsSheetName)
        {
            var rngA1 = sheet.Range["A1"];
            string oldContentsSheetName = Convert.ToString(rngA1.Value);
            if (oldContentsSheetName != "К оглавлению" && oldContentsSheetName != "[Перейти в карту выгрузки]")
            {
                rngA1.EntireRow.Insert(Excel.XlDirection.xlUp);
            }

            sheet.Range["A1"].FormulaLocal = $@"=ГИПЕРССЫЛКА(""#{contentsSheetName}!A1"";""К оглавлению"")";
        }
        public static Excel.Workbook AddNewWorkbook()
        {
            return Globals.ThisAddIn.Application.Workbooks.Add();
        }

        public static Excel.Workbook OpenWorkbook(string filePath, bool isReadOnly = false)
        {
            return Globals.ThisAddIn.Application.Workbooks.Open(filePath, ReadOnly: isReadOnly);
        }

        public static Excel.Workbook GetActiveWorkbook()
        {
            return (Excel.Workbook)Globals.ThisAddIn.Application.ActiveWorkbook;
        }

        public static Excel.Worksheet GetActiveWorksheet()
        {
            return (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
        }

        public static Excel.Sheets GetAllSheetsOfActiveWorkbook()
        {
            return (Excel.Sheets)Globals.ThisAddIn.Application.ActiveWorkbook.Sheets;
        }

        public static Excel.Worksheet GetSheetByName(string sheetName)
        {
            Excel.Worksheet sheet;
            try
            {
                sheet = (Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[sheetName];
            }
            catch
            {
                sheet = null;
            }
            return sheet;
        }

        public static Excel.Range SelectRange(string message = "Выберите диапазон")
        {
            var tmpRange = Globals.ThisAddIn.Application.InputBox(message, "Выбор диапазона", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, 8);

            if (tmpRange is Excel.Range) return (Excel.Range) tmpRange;

            return null;

        }

        public static Excel.Range GetRangeFromString(string rngStr, Excel.Worksheet sheet = null)
        {
            if (rngStr.Contains("!"))
            {
                var tmpArr = rngStr.Split('!');
                sheet = GetSheetByName(tmpArr[0].Replace("'", ""));
                rngStr = tmpArr[1];
            }

            if (sheet is null) sheet = GetActiveWorksheet();

            return sheet.Range[rngStr];
        }
        
        public static Excel.Worksheet GetWorksheetFromStringRangeAnnotation(string rngString)
        {
            return GetSheetByName(rngString.Split('!')[0].Replace("'", ""));
        }

        public static Excel.Worksheet GetSheetByName(string sheetName, Excel.Workbook wb)
        {
            Excel.Worksheet sheet;
            try
            {
                sheet = (Excel.Worksheet)wb.Worksheets[sheetName];
            }
            catch
            {
                sheet = null;
            }
            return sheet;
        }

        public static Excel.Worksheet GetSheetByIndex(int sheetIndex)
        {
            Excel.Worksheet sheet;
            try
            {
                sheet = (Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[sheetIndex];
            }
            catch
            {
                sheet = null;
            }
            return sheet;
        }

        public static int GetLastRow(Excel.Worksheet sheet, int columnNumber)
        {
            Excel.Range cells = (Excel.Range)sheet.Cells[sheet.Rows.Count, columnNumber];
            int countOfExcelCells = cells.End[Excel.XlDirection.xlUp].Row;

            return countOfExcelCells;
        }

        public static string[,] GetRowValues(Excel.Worksheet sheet, int rowNumber)
        {
            int lastColumn = GetLastColumnNumberInRow(sheet, rowNumber);
            return GetStringArrayFromRange(sheet.Range[sheet.Cells[rowNumber, 1], sheet.Cells[rowNumber, lastColumn]]);
        }

        /// <summary>
        /// Возвращает диапазон строки до последнего заполненного столбца
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowNumber"></param>
        /// <returns></returns>
        public static Excel.Range GetRowRange(Excel.Worksheet sheet, int rowNumber)
        {
            int lastColumn = GetLastColumnNumberInRow(sheet, rowNumber);
            return sheet.Range[sheet.Cells[rowNumber, 1], sheet.Cells[rowNumber, lastColumn]];
        }
        /// <summary>
        /// Возвращает диапазон строк до последнего заполненного столбца
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="firstRowNumber"></param>
        /// <param name="rowsCount"></param>
        /// <returns></returns>
        public static Excel.Range GetRowsRange(Excel.Worksheet sheet, int firstRowNumber, int rowsCount)
        {
            int lastColumn = GetLastColumnInSheet(sheet);
            return sheet.Range[sheet.Cells[firstRowNumber, 1], sheet.Cells[firstRowNumber + rowsCount, lastColumn]];
        }

        /// <summary>
        /// Возвращает диапазон столбца от первой указанной строки до конца столбца
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="columnNumber"></param>
        /// <param name="firstRow"></param>
        /// <returns></returns>
        public static Excel.Range GetColumnRange(Excel.Worksheet sheet, int columnNumber, int firstRow = 1, int lastRow = 0)
        {
            if (lastRow == 0) lastRow = GetLastRow(sheet, columnNumber);
            return sheet.Range[sheet.Cells[firstRow, columnNumber], sheet.Cells[lastRow, columnNumber]];
        }

        /// <summary>
        /// Возвращает значения столбца от первой указанной строки до конца столбца
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="columnNumber"></param>
        /// <param name="firstRow"></param>
        /// <returns></returns>
        public static System.Array GetColumnValues(Excel.Worksheet sheet, int columnNumber, int firstRow = 1)
        {
            var rng = GetColumnRange(sheet, columnNumber, firstRow);
            return (System.Array)rng.Value;
        }

        public static dynamic [,] GetSheetValues(Excel.Worksheet sheet, int firstRow = 1)
        {
            int lastColumn = GetLastColumnInSheet(sheet);
            int lastRow = GetLastRowInSheet(sheet);
            var rng = sheet.Range[sheet.Cells[firstRow, 1], sheet.Cells[lastRow, lastColumn]];
            return (dynamic[,])rng.Value;
        }

        public static dynamic[,] GetSheetValues(Excel.Worksheet sheet, int firstRow, int columnNumberToFindLastValue)
        {
            int lastColumn = GetLastColumnInSheet(sheet);
            int lastRow = GetLastRow(sheet, columnNumberToFindLastValue);
            var rng = sheet.Range[sheet.Cells[firstRow, 1], sheet.Cells[lastRow, lastColumn]];
            return (dynamic[,])rng.Value;
        }

        public static int GetLastColumnNumberInRow(Excel.Worksheet sheet, int rowNumber)
        {
            Excel.Range cells = (Excel.Range)sheet.Cells[rowNumber, sheet.Columns.Count];

            int countOfExcelCells = cells.End[Excel.XlDirection.xlToLeft].Column;

            return countOfExcelCells;
        }

        internal static string GetSheetNameFromHyperlinkFormulaKeyCollector(string formula)
        {
            if (!formula.Contains("HYPERLINK")) return null;
            return formula.Replace("=HYPERLINK(\"#\"&\"'", "").Replace("=HYPERLINK(\"#'","").Split(separator: new char[1] { '\'' }, count: 2)[0];
        }

        internal static string GetSheetNameFromHyperlinkFormula(string formula)
        {
            throw new NotImplementedException();
        }

        public static string GetLastValueInColumn(Excel.Worksheet sheet, int columnNumber)
        {
            Excel.Range cells = (Excel.Range)sheet.Cells[sheet.Rows.Count, columnNumber];
            return Convert.ToString(cells.End[Excel.XlDirection.xlUp].Value);
        }

        public static Excel.Worksheet AddSheet(string sheetName, int before = 1)
        {
            Excel.Worksheet newWorksheet;
            newWorksheet = (Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets.Add(Globals.ThisAddIn.Application.Worksheets[before]);
            newWorksheet.Name = sheetName;
            return newWorksheet;
        }

        public static Excel.Worksheet AddSheetAfter(string sheetName, int after = 1)
        {
            Excel.Worksheet newWorksheet;
            newWorksheet = (Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets.Add(After: Globals.ThisAddIn.Application.Worksheets[after]);
            string newShName = CleanStringForSheetName(sheetName);
            try { 
            newWorksheet.Name = newShName;
            } catch (Exception)
            {
                MessageBox.Show($"Внимание! Ошибка в переименовывании листа {newWorksheet.Name} в {sheetName}! Скорее всего, проблема в недопустимых символах!");
            }
            return newWorksheet;
        }

        public static string CleanStringForSheetName(string sheetName)
        {
            string[] badSymbols = { "/", "\\", ":", "?", "[", "]", "*" };
            foreach (string symbol in badSymbols) sheetName = sheetName.Replace(symbol, "");

            if (sheetName.Length > 30) sheetName = sheetName.Substring(0, 30);
            if (IsSheetExist(sheetName))
            {
                sheetName = sheetName.Substring(0, 25);
                sheetName += $"({Globals.ThisAddIn.Application.Worksheets.Count})";
            }

            return sheetName;
        }

        public static Excel.Worksheet GetOrCreateSheet(string sheetName)
        {
            var sheet = GetSheetByName(sheetName);
            if (sheet is null) sheet = AddSheet(sheetName);
            return sheet;
        }

        public static bool IsSheetExist(string sheetName)
        {
            var sheet = GetSheetByName(sheetName);
            if (sheet is null) return false;
            return true;
        }
        public static bool IsSheetExist(string sheetName, Excel.Workbook wb)
        {
            var sheet = GetSheetByName(sheetName, wb);
            if (sheet is null) return false;
            return true;
        }

        public static Dictionary<int, string> CreateDictionaryOfSheets(Excel.Sheets sheets)
        {
            var dictionary = new Dictionary<int, string>();

            foreach (Excel.Worksheet sheet in sheets)
            {
                dictionary.Add(sheet.Index, sheet.Name);
            }

            return dictionary;
        }

        public static Dictionary<string, int> CreateDictionaryOfSheetsKeyName(Excel.Sheets sheets)
        {
            var dictionary = new Dictionary<string, int>();

            foreach (Excel.Worksheet sheet in sheets)
            {
                dictionary.Add(sheet.Name, sheet.Index);
            }

            return dictionary;
        }

        public static string[,] ConvertListToArrayForExcel(List<string> inputList, string prefix = "", string postfix = "")
        {
            string[,] arrToWrite = new string[inputList.Count, 1];

            for (int i = 0; i < inputList.Count; i++)
            {
                arrToWrite[i, 0] = prefix + inputList[i] + postfix;
            }

            return arrToWrite;
        }

        public static void AddHyperlinkToSheet(Excel.Worksheet sheet, string cellAddress, string hyperAddress, string TextToDisplay)
        {
            sheet.Hyperlinks.Add(sheet.Range[cellAddress], "", hyperAddress, "", TextToDisplay);
        }

        public static bool IsItUrl(string val)
        {
            return Uri.IsWellFormedUriString(val, UriKind.Absolute);
        }

        public static string GetSheetNameFromHyperlink(Excel.Hyperlink link)
        {
            int lastMark = link.SubAddress.LastIndexOf("'");
            if (lastMark > 1) return link.SubAddress.Substring(1, lastMark - 1);

            lastMark = link.SubAddress.LastIndexOf("!");
            if (lastMark > 1) return link.SubAddress.Substring(0, lastMark);

            return null;
        }

        public static int GetLastColumnInSheet(Excel.Worksheet sheet)
        {
            return (sheet.UsedRange.Columns.Count + sheet.UsedRange.Column - 1);

        }

        public static int GetLastRowInSheet(Excel.Worksheet sheet)
        {
            return (sheet.UsedRange.Rows.Count + sheet.UsedRange.Row - 1);

        }
        public static Excel.Worksheet GetSheetFromHyperlink(Excel.Hyperlink link)
        {
            string shName = GetSheetNameFromHyperlink(link);
            if (shName is null) return null;
            return GetSheetByName(shName);
        }

        public static void InsertHyperlinkExcelMethod(Excel.Worksheet sheet, int row, int column, string address, string subAddress, string TextToDisplay)
        {
            sheet.Hyperlinks.Add(sheet.Cells[row, column], address, subAddress, "", TextToDisplay);
        }

        public static void InsertHyperlinkFormulaMethod(Excel.Worksheet sheet, int row, int column, string hyperlinkAddress, string TextToDisplay)
        {
            string cellAddress = GetCellAddress(row, column);
            sheet.Range[cellAddress].Value = $@"=HYPERLINK(""{hyperlinkAddress}"",""{TextToDisplay}"")";
            // sheet.Range[cellAddress].Formula = sheet.Range[cellAddress].Value;
        }

        public static string GetCellAddress(int row, int col)
        {
            StringBuilder sb = new StringBuilder();
            col--;
            do
            {
                sb.Insert(0, (char)('A' + (col % 26)));
                col /= 26;
            } while (col > 0);
            sb.Append(row);
            return sb.ToString();
        }

        public static HashSet<string> GetUniqValuesFromColumn(Excel.Worksheet sheet, int columnNumber, int headerRow)
        {
            var lastRow = GetLastRow(sheet, columnNumber);

            int rowsCount = lastRow - headerRow;

            string firstCell = GetCellAddress(headerRow + 1, columnNumber);
            string secondCell = GetCellAddress(lastRow, columnNumber);


            var values = GetValuesArrayFromRange(sheet.Range[firstCell + ":" + secondCell]);

            var set = new HashSet<string>();

            for (int row = 1; row <= rowsCount; row++)
            {
                string value = values.GetValue(row, 1)?.ToString();
                set.Add(value);
            }

            return set;

        }

        public static System.Array GetValuesArrayFromRange(Excel.Range rng)
        {
            return (System.Array)rng.Value;
        }

        public static string[][] GetStringArrayOfArraysFromRange(Excel.Range rng)
        {

            var oldArray = (System.Array)rng.Value;
            //string[,] arr = Array.ConvertAll((object[,])oldArray, Convert.ToString);
            if (oldArray == null) return new string[0][];

            string[][] newArr = new string[oldArray.GetLength(0)][];

            int secondDim = oldArray.GetLength(1);

            for (int i = 1; i <= oldArray.GetLength(0); i++)
            {
                string[] tmpArr = new string[secondDim];

                for (int j = 1; j <= secondDim; j++)
                {
                    tmpArr[j - 1] = oldArray.GetValue(i, j)?.ToString();
                }
                newArr[i - 1] = tmpArr;
            }


            return newArr;
        }

        public static string[,] GetStringArrayFromRange(Excel.Range rng)
        {
            if (rng.Value is string) return new string[,] { { (string)rng.Value } };
            var oldArray = (System.Array)rng.Value;
            if (oldArray == null) return new string[0, 0];
            //string[,] arr = Array.ConvertAll((object[,])oldArray, Convert.ToString);
            int secondDim = oldArray.GetLength(1);

            string[,] newArr = new string[oldArray.GetLength(0), secondDim];

            for (int i = 1; i <= oldArray.GetLength(0); i++)
            {
                for (int j = 1; j <= secondDim; j++)
                {
                    newArr[i - 1, j - 1] = oldArray.GetValue(i, j)?.ToString();
                }
            }

            return newArr;
        }

        public static string ExcelColumnFromNumber(int column)
        {
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
            }
            return columnString;
        }

        public static int NumberFromExcelColumn(string column)
        {
            int retVal = 0;
            string col = column.ToUpper();
            for (int iChar = col.Length - 1; iChar >= 0; iChar--)
            {
                char colPiece = col[iChar];
                int colNum = colPiece - 64;
                retVal = retVal + colNum * (int)Math.Pow(26, col.Length - (iChar + 1));
            }
            return retVal;
        }


        public static string[,] GetFilteredArrayFromStringArray(string[,] fullArray, string stringToFind, int columnNumber, int indexOfArraysFirstElement = 0)
        {
            string[,] filteredArray = new string[fullArray.GetLength(0), fullArray.GetLength(1)];
            int counter = 0;
            for (int i = indexOfArraysFirstElement; i < fullArray.GetLength(0); i++)
            {
                if (fullArray[i, columnNumber] == stringToFind)
                {
                    for (int j = indexOfArraysFirstElement; j < fullArray.GetLength(1); j++)

                    {
                        filteredArray[counter, j] = fullArray[i, j];
                    }
                    counter++;
                }
            }

            return filteredArray;

        }

        /// <summary>
        /// Возвращает реальный заполненный диапазон страницы (от A1 до конца)
        /// </summary>
        /// <param name="sheet"></param>
        /// <returns></returns>
        public static Excel.Range GetAllSheetRange(Excel.Worksheet sheet)
        {
            var lastRow = GetLastRowInSheet(sheet);
            var lastColumn = GetLastColumnInSheet(sheet);
            return sheet.Range[sheet.Cells[1, 1], sheet.Cells[lastRow, lastColumn]];
        }


        public static void AddColumnAfter(Excel.Worksheet sheet, int columnAfter)
        {
            sheet.Range[ExcelFunctions.ExcelColumnFromNumber(columnAfter) + "1"].EntireColumn.Insert();
        }

        public static void AddColumnsAfter(Excel.Worksheet sheet, int columnAfter, int columnsQuantity)
        {
            var rng = sheet.Range[ExcelFunctions.ExcelColumnFromNumber(columnAfter + 1) + "1"];
            for (int i = 0; i <= columnsQuantity; i++) rng.EntireColumn.Insert();
        }

        public static Excel.Range GetCellAsRange(Excel.Worksheet sheet, int row, int column)
        {
            return sheet.Cells[row, column] as Excel.Range;
        }

        public static bool IsNoBooksOpened()
        {
            Excel.Workbooks workbooks = Globals.ThisAddIn.Application.Workbooks;
            if (workbooks.Count < 1) return true;
            return false;
        }

        public static bool IsNoBooksOpenedWithMessage()
        {
            if (IsNoBooksOpened())
            {
                MessageBox.Show("Откройте хотя бы одну книгу для использования инструмента!");
                return true;
            }
            return false;
        }

        public static Excel.Range GetActiveCell()
        {
            return Globals.ThisAddIn.Application.ActiveCell;
        }

        public static void UnmergeCellsInRange(Excel.Range rng)
        {
            rng.UnMerge();
        }

        public static void UnmergeCellsInSheet(Excel.Worksheet sheet)
        {
            sheet.UsedRange.UnMerge();
        }

        public static void ClearSortOnSheet(Excel.Worksheet sheet)
        {
            sheet.Sort.SortFields.Clear();
        }

        public static void ClearAutofilter(Excel.Worksheet sheet)
        {
            if (sheet.AutoFilter != null && sheet.AutoFilterMode == true)
            {
                sheet.AutoFilter.ShowAllData();
            }

            if (sheet.AutoFilter != null && sheet.AutoFilter.Sort.SortFields.Count > 0)
            {
                sheet.AutoFilter.Sort.SortFields.Clear();
            }

            sheet.AutoFilterMode = false;
            sheet.EnableAutoFilter = false;

        }
        /// <summary>
        /// Возвращает используемый диапазон на странице, опционально можно обрезать первые строки, указав firstRow
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="firstRow"></param>
        /// <returns></returns>
        public static Excel.Range GetSheetUsedRange(Excel.Worksheet sheet, int firstRow = 1)
        {
            if (firstRow == 1) return sheet.UsedRange;
            string lastColumn = ExcelColumnFromNumber(GetLastColumnInSheet(sheet));
            int lastRow = GetLastRowInSheet(sheet);
            return sheet.Range[$"A{firstRow}:{lastColumn}{lastRow}"];
        }
        /// <summary>
        /// Фнукция сортировки стообца , не очень рабочая - ошибка из-за .Orientatins, непонятно, в чем проблема
        /// Лучше использовать функцию SortColumnWithAutofilter
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rng"></param>
        /// <param name="sortOrder"></param>
        public static void AddSortToRange(Excel.Worksheet sheet, Excel.Range rng, Excel.XlSortOrder sortOrder)
        {
            Excel.Sort sheetSort = sheet.Sort;
            //sheetSort.SortFields.Add(Key: rng, Order: sortOrder);

            sheetSort.SortFields.Add(Key: rng, SortOn: Excel.XlSortOn.xlSortOnValues, Order: sortOrder,
                DataOption: Excel.XlSortDataOption.xlSortNormal);

            sheetSort.SetRange(sheet.UsedRange);

            //sheetSort.Header = Excel.XlYesNoGuess.xlYes;
            //sheetSort.MatchCase = false;
            sheetSort.Orientation = Excel.XlSortOrientation.xlSortRows;
            //sheetSort.SortMethod = Excel.XlSortMethod.xlPinYin;

            sheetSort.Apply();

        }

        public static bool SortColumnWithAutofilter(Excel.Worksheet sheet, Excel.XlSortOrder sortOrder, int ColumnNumber, int headersRow = 1)
        {
            Excel.Range rng = ExcelFunctions.GetColumnRange(sheet, columnNumber: ColumnNumber, firstRow: headersRow);
            try
            {
                sheet.Range[$"A{headersRow}"].AutoFilter(1);
                sheet.AutoFilter.ApplyFilter();

                sheet.AutoFilter.Sort.SortFields.Add(Key: rng, SortOn: Excel.XlSortOn.xlSortOnValues, Order: sortOrder);
                sheet.AutoFilter.ApplyFilter();
            }
            catch (Exception)
            {
                MessageBox.Show($"Ошибка! Не удалось применить фильтр на листе '{sheet.Name}' Примените фильтр самостоятельно!");
                return false;
            }

            return true;
        }

        public static void UnlinkListObjectsOnSheet(Excel.Worksheet sheet)
        {
            Excel.ListObjects listObjects = sheet.ListObjects;
            foreach (Excel.ListObject item in listObjects)
            {
                item.Unlist();
            }
        }

        public static bool IsSignificantValuesInRange(Excel.Range rng)
        {
            dynamic vals = rng.Value;
            if (vals is string) return vals.Length > 0;
            if (MyExtensions.IsObjectNumber(vals)) return vals > 0;

            foreach (dynamic val in vals)
            {
                if (MyExtensions.IsDynamicSignificant(val) ?? false) return true;
            }

            return false;
        }
    }
}
