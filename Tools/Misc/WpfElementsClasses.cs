﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WebtronicsAddIn.Tools.Misc
{
    public static class SortDataByDictionary
    {
        private static StringDictionary _sortDataBy;
        static SortDataByDictionary()
        {
            _sortDataBy = new StringDictionary();
            _sortDataBy.Add("asc", "А-Я");
            _sortDataBy.Add("desc", "Я-А");
        }
        public static StringDictionary SortDataBy
        {
            get
            {
                return _sortDataBy;
            }
        }
    }
}
