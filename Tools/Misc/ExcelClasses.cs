﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebtronicsAddIn.Tools.Functions
{
    public class ExcelClasses
    {

        public class Cell
        {
            public int row { get; set; }

            public int column { get; set; }


            private string _ColumnLetter;
            public string ColumnLetter
            {
                get
                {
                    if (_ColumnLetter is null) _ColumnLetter = ExcelFunctions.ExcelColumnFromNumber(column);
                    return _ColumnLetter;
                }
                set { _ColumnLetter = value; }
            }


            public String stringValue { get; set; }

            public Boolean IsChecked { get; set; }

            public Cell(bool _IsChecked = false)
            {
                IsChecked = _IsChecked;
            }

            public Cell(Excel.Range cell, bool _IsChecked = false)
            {
                row = cell.Row;
                column = cell.Column;
                stringValue = Convert.ToString(cell.Value);
                IsChecked = _IsChecked;
            }



            /// <summary>
            /// Возвращает строковое представление адреса ячейки, напр. A1
            /// </summary>
            /// <returns></returns>
            public string getCellAddress()
            {
                StringBuilder sb = new StringBuilder();
                int tmpColumn = this.column - 1;
                do
                {
                    sb.Insert(0, (char)('A' + (tmpColumn % 26)));
                    tmpColumn /= 26;
                } while (tmpColumn > 0);
                sb.Append(row);
                return sb.ToString();
            }

        }


        public class Cells
        {
            public Cell[,] list;


            /// <summary>
            /// Конструктор, принимающий диапазон
            /// </summary>
            /// <param name="rng">Диапазон Excel.Range</param>
            public Cells(Excel.Range rng, Boolean IsChecked = false)
            {
                var input = rng.Value;
                if (input == null) return;
                var type = input.GetType();
                if (!type.IsArray)
                {
                    list = new Cell[1, 1];
                    list[0, 0] = new Cell(cell: rng, _IsChecked: IsChecked);
                    return;
                };

                var firstColumn = rng.Column;
                var firstRow = rng.Row;

                var inputArray = (System.Array)input;
                int rowsCount = inputArray.GetLength(0);
                int columnsCount = inputArray.GetLength(1);

                list = new Cell[inputArray.GetLength(0), inputArray.GetLength(1)];

                for (int i = 1; i <= rowsCount; i++)
                {
                    for (int j = 1; j <= columnsCount; j++)
                    {
                        Cell cell = new Cell() { column = j + firstColumn - 1, row = i + firstRow - 1, stringValue = inputArray.GetValue(i, j)?.ToString(), IsChecked = IsChecked };
                        list[i - 1, j - 1] = cell;
                    }
                }
            }


            public ObservableCollection<Cell> createObservableCollection()
            {
                ObservableCollection<Cell> collection = new ObservableCollection<Cell>();
                foreach (var cell in this.list)
                {
                    if (cell.stringValue != null && cell.stringValue.Length > 0) collection.Add(cell);
                }
                return collection;
            }

            public CollectionViewSource createCollectionViewSource()
            {
                var observableColl = this.createObservableCollection();

                var viewSource = new CollectionViewSource();
                viewSource.Source = observableColl;
                return viewSource;
            }

            public void SetIsCheckedForAllCells(bool NewIsChecked)
            {
                foreach (var cell in this.list) cell.IsChecked = NewIsChecked;
            }

            /// <summary>
            /// Возвращает словарь с уникальными непустыми строками в словаре
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, Cell> GetDictionaryOfUniqCells()
            {

                var NotEmptyCells = new Dictionary<string, Cell>();
                foreach (var cell in this.list)
                {
                    if (cell.stringValue != null && cell.stringValue.Length > 0 && !NotEmptyCells.ContainsKey(cell.stringValue)) NotEmptyCells.Add(cell.stringValue, cell);
                }
                return NotEmptyCells;
            }

        }

        /// <summary>
        /// Класс Cell, наследуемый от Cell, с именем содержащейся в нем ссылки на лист (изначально делалось под KC суммирование частотностей)
        /// </summary>
        public class CellWithLinkToSheet : Cell
        {
            public string SheetName { get; set; }
            public ExcelSheets SheetRef { get; set; }

            public CellWithLinkToSheet(bool _IsChecked = false) : base(_IsChecked)
            {

            }

            public CellWithLinkToSheet(Excel.Hyperlink hyperCell = null, bool _IsChecked = false) : base(cell: hyperCell.Range, _IsChecked: _IsChecked)
            {
                SheetName = ExcelFunctions.GetSheetNameFromHyperlink(hyperCell);
            }

            public CellWithLinkToSheet(Excel.Range cell, bool isKeyCollectorFormula, bool _IsChecked = false) : base(cell: cell, _IsChecked: _IsChecked)
            {
                if (isKeyCollectorFormula)
                {
                    SheetName = ExcelFunctions.GetSheetNameFromHyperlinkFormulaKeyCollector(Convert.ToString(cell.Formula));
                }
                else
                {
                    SheetName = ExcelFunctions.GetSheetNameFromHyperlinkFormula(Convert.ToString(cell.Formula));
                }
            }


        }


        public class CellsWithLinkToSheet
        {
            public List<CellWithLinkToSheet> list;


            /// <summary>
            /// Создает и хранит список CellWithLinkToSheet
            /// </summary>
            /// <param name="rng">Диапазон Excel.Range</param>
            public CellsWithLinkToSheet(Excel.Range rng, Boolean IsChecked = false, bool isKeyCollectorFormula = false)
            {
                list = new List<CellWithLinkToSheet>();
                var input = rng.Formula;
                if (input == null) return;
                var type = input.GetType();
                if (!type.IsArray)
                {
                    list.Add(new CellWithLinkToSheet(cell: rng, isKeyCollectorFormula: isKeyCollectorFormula, _IsChecked: IsChecked));
                    return;
                };

                var firstColumn = rng.Column;
                var firstRow = rng.Row;

                var inputArray = (System.Array)input;
                int rowsCount = inputArray.GetLength(0);
                int columnsCount = inputArray.GetLength(1);

                for (int i = 1; i <= rowsCount; i++)
                {
                    for (int j = 1; j <= columnsCount; j++)
                    {
                        string formula = inputArray.GetValue(i, j)?.ToString();
                        string sheetName = ExcelFunctions.GetSheetNameFromHyperlinkFormulaKeyCollector(formula);
                        if (sheetName is null) continue;

                        list.Add(new CellWithLinkToSheet(_IsChecked: IsChecked) { column = j + firstColumn - 1, row = i + firstRow - 1, stringValue = formula, SheetName = sheetName });

                    }
                }

            }

            public CellsWithLinkToSheet(Excel.Hyperlinks links, Boolean IsChecked = false)
            {
                list = new List<CellWithLinkToSheet>();
                foreach (Excel.Hyperlink hyperCell in links)
                {
                    list.Add(new CellWithLinkToSheet(hyperCell: hyperCell, _IsChecked: IsChecked));
                }

            }

        }
        public class Column
        {
            public int number { get; set; }

            public string header { get; set; }
            public Boolean IsChecked { get; set; }
            public string key { get; set; }

            private string _letter = "";
            public string letter
            {
                get
                {
                    if (_letter.Length < 1) _letter = ExcelFunctions.ExcelColumnFromNumber(this.number);
                    return _letter;
                }
                set { _letter = value; }
            }
            
            private float _width = -1;
            public float width
            {
                get { return _width; }
                set { _width = value; }
            }

        }

        public class Columns
        {
            public Column[] list;


            /// <summary>
            /// Конструктор, принимающий диапазон
            /// </summary>
            /// <param name="rng">Диапазон Excel.Range</param>
            public Columns(int countOfColumns, bool IsCheckedColumns)
            {
                list = new Column[countOfColumns];
                for (int i = 1; i <= countOfColumns; i++)
                {
                    list[i - 1] = new Column() { number = i, IsChecked = IsCheckedColumns };
                }
            }


            public ObservableCollection<Column> createObservableCollection()
            {
                ObservableCollection<Column> collection = new ObservableCollection<Column>();
                foreach (var column in this.list)
                {
                   collection.Add(column);
                }
                return collection;
            }

            public CollectionViewSource createCollectionViewSource()
            {
                var observableColl = this.createObservableCollection();

                var viewSource = new CollectionViewSource();
                viewSource.Source = observableColl;
                return viewSource;
            }

            public void SetIsCheckedForAllColumns(bool NewIsChecked)
            {
                foreach (var column in this.list) column.IsChecked = NewIsChecked;
            }

            public void GetColumnsWidthFromSheet(Excel.Worksheet sheet = null)
            {
                if (sheet == null) sheet = ExcelFunctions.GetActiveWorksheet();
                foreach (var column in this.list) column.width = (float) ExcelFunctions.GetRangeFromString(column.letter + "1").ColumnWidth;
            }
            public void SetColumnsWidthToSheet(Excel.Worksheet sheet = null, bool IsOnlyCheckedColumns = false)
            {
                if (sheet == null) sheet = ExcelFunctions.GetActiveWorksheet();
                foreach (var column in this.list)
                {
                    if (IsOnlyCheckedColumns)
                    {
                        if (column.IsChecked) ExcelFunctions.GetRangeFromString(column.letter + "1").ColumnWidth = column.width;
                    } 
                    else ExcelFunctions.GetRangeFromString(column.letter + "1").ColumnWidth = column.width;
                }
            }

            public void SetWidthToColumns(float newWidth)
            {
                foreach (var column in this.list) column.width = newWidth;
            }
        }
    }
}

