﻿namespace WebtronicsAddIn
{
    partial class webtronicsRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public webtronicsRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTabWebtronics = this.Factory.CreateRibbonTab();
            this.mainRibbonTools = this.Factory.CreateRibbonGroup();
            this.contentsCreateRibbon = this.Factory.CreateRibbonButton();
            this.UrlToHyperlinkRibbonButton = this.Factory.CreateRibbonButton();
            this.filterToNewSheetRibbonButton = this.Factory.CreateRibbonButton();
            this.BooksMergeRibbonButton = this.Factory.CreateRibbonButton();
            this.SortColumnsOnAllSheetsButton = this.Factory.CreateRibbonButton();
            this.ColumnsWidthChanger_RibbonButton = this.Factory.CreateRibbonButton();
            this.KC_TOOLS = this.Factory.CreateRibbonGroup();
            this.KeyCollectorRibbonSummFrequency = this.Factory.CreateRibbonButton();
            this.TransormSheetForGoogleSheets = this.Factory.CreateRibbonButton();
            this.button1 = this.Factory.CreateRibbonButton();
            this.Button_KC_CreateMenu = this.Factory.CreateRibbonButton();
            this.group_misc = this.Factory.CreateRibbonGroup();
            this.WordsHighligherRibbonButton = this.Factory.CreateRibbonButton();
            this.AboutGroup = this.Factory.CreateRibbonGroup();
            this.AboutButton = this.Factory.CreateRibbonButton();
            this.RemoveColumnsWithoutData_RibbonButton = this.Factory.CreateRibbonButton();
            this.mainTabWebtronics.SuspendLayout();
            this.mainRibbonTools.SuspendLayout();
            this.KC_TOOLS.SuspendLayout();
            this.group_misc.SuspendLayout();
            this.AboutGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabWebtronics
            // 
            this.mainTabWebtronics.Groups.Add(this.mainRibbonTools);
            this.mainTabWebtronics.Groups.Add(this.KC_TOOLS);
            this.mainTabWebtronics.Groups.Add(this.group_misc);
            this.mainTabWebtronics.Groups.Add(this.AboutGroup);
            this.mainTabWebtronics.Label = "Webtronics";
            this.mainTabWebtronics.Name = "mainTabWebtronics";
            // 
            // mainRibbonTools
            // 
            this.mainRibbonTools.Items.Add(this.contentsCreateRibbon);
            this.mainRibbonTools.Items.Add(this.UrlToHyperlinkRibbonButton);
            this.mainRibbonTools.Items.Add(this.filterToNewSheetRibbonButton);
            this.mainRibbonTools.Items.Add(this.BooksMergeRibbonButton);
            this.mainRibbonTools.Items.Add(this.SortColumnsOnAllSheetsButton);
            this.mainRibbonTools.Items.Add(this.ColumnsWidthChanger_RibbonButton);
            this.mainRibbonTools.Label = "Инструменты";
            this.mainRibbonTools.Name = "mainRibbonTools";
            // 
            // contentsCreateRibbon
            // 
            this.contentsCreateRibbon.Label = "Создать \"Оглавление\"";
            this.contentsCreateRibbon.Name = "contentsCreateRibbon";
            this.contentsCreateRibbon.OfficeImageId = "Numbering";
            this.contentsCreateRibbon.ScreenTip = "Создать оглавление";
            this.contentsCreateRibbon.ShowImage = true;
            this.contentsCreateRibbon.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.contentsCreateRibbon_Click);
            // 
            // UrlToHyperlinkRibbonButton
            // 
            this.UrlToHyperlinkRibbonButton.Label = "Сделать URL кликабельными";
            this.UrlToHyperlinkRibbonButton.Name = "UrlToHyperlinkRibbonButton";
            this.UrlToHyperlinkRibbonButton.OfficeImageId = "HyperlinksVerify";
            this.UrlToHyperlinkRibbonButton.ScreenTip = "Делает URL кликабельными";
            this.UrlToHyperlinkRibbonButton.ShowImage = true;
            this.UrlToHyperlinkRibbonButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.UrlToHyperlinkRibbonButton_Click);
            // 
            // filterToNewSheetRibbonButton
            // 
            this.filterToNewSheetRibbonButton.Label = "Отфильтровать на новый лист";
            this.filterToNewSheetRibbonButton.Name = "filterToNewSheetRibbonButton";
            this.filterToNewSheetRibbonButton.OfficeImageId = "CalendarToolExportAllAppointments";
            this.filterToNewSheetRibbonButton.ScreenTip = "Применить фильтр по значениям и копировать отфильтрованные значения на отдельные " +
    "листы";
            this.filterToNewSheetRibbonButton.ShowImage = true;
            this.filterToNewSheetRibbonButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.filterToNewSheetRibbonButton_Click);
            // 
            // BooksMergeRibbonButton
            // 
            this.BooksMergeRibbonButton.Label = "Объединение книг";
            this.BooksMergeRibbonButton.Name = "BooksMergeRibbonButton";
            this.BooksMergeRibbonButton.OfficeImageId = "Copy";
            this.BooksMergeRibbonButton.ScreenTip = "Объединение нескольких выбранных книг в одну";
            this.BooksMergeRibbonButton.ShowImage = true;
            this.BooksMergeRibbonButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.MergeBooksRibbonButton_Click);
            // 
            // SortColumnsOnAllSheetsButton
            // 
            this.SortColumnsOnAllSheetsButton.Label = "Сортировка столбцов";
            this.SortColumnsOnAllSheetsButton.Name = "SortColumnsOnAllSheetsButton";
            this.SortColumnsOnAllSheetsButton.OfficeImageId = "SortUp";
            this.SortColumnsOnAllSheetsButton.ScreenTip = "Сортировка столбцов на выбранных листах";
            this.SortColumnsOnAllSheetsButton.ShowImage = true;
            this.SortColumnsOnAllSheetsButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SortColumnsOnAllSheetsButton_Click);
            // 
            // ColumnsWidthChanger_RibbonButton
            // 
            this.ColumnsWidthChanger_RibbonButton.Label = "Изменение ширины столбцов";
            this.ColumnsWidthChanger_RibbonButton.Name = "ColumnsWidthChanger_RibbonButton";
            this.ColumnsWidthChanger_RibbonButton.OfficeImageId = "ColumnWidth";
            this.ColumnsWidthChanger_RibbonButton.ScreenTip = "Массовое изменение ширины столбцов на выбранных листах";
            this.ColumnsWidthChanger_RibbonButton.ShowImage = true;
            this.ColumnsWidthChanger_RibbonButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ColumnsWidthChangerButton_Click);
            // 
            // KC_TOOLS
            // 
            this.KC_TOOLS.Items.Add(this.KeyCollectorRibbonSummFrequency);
            this.KC_TOOLS.Items.Add(this.TransormSheetForGoogleSheets);
            this.KC_TOOLS.Items.Add(this.button1);
            this.KC_TOOLS.Items.Add(this.Button_KC_CreateMenu);
            this.KC_TOOLS.Label = "Выгрузка из KC";
            this.KC_TOOLS.Name = "KC_TOOLS";
            // 
            // KeyCollectorRibbonSummFrequency
            // 
            this.KeyCollectorRibbonSummFrequency.Label = "Суммировать частотность";
            this.KeyCollectorRibbonSummFrequency.Name = "KeyCollectorRibbonSummFrequency";
            this.KeyCollectorRibbonSummFrequency.OfficeImageId = "OutlineExpand";
            this.KeyCollectorRibbonSummFrequency.ShowImage = true;
            this.KeyCollectorRibbonSummFrequency.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.KeyCollectorRibbonSummFrequency_Click);
            // 
            // TransormSheetForGoogleSheets
            // 
            this.TransormSheetForGoogleSheets.Label = "Прогнозирование трафика и конверсий";
            this.TransormSheetForGoogleSheets.Name = "TransormSheetForGoogleSheets";
            this.TransormSheetForGoogleSheets.OfficeImageId = "TracePrecedents";
            this.TransormSheetForGoogleSheets.ShowImage = true;
            this.TransormSheetForGoogleSheets.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.TransormSheetForGoogleSheets_Click);
            // 
            // button1
            // 
            this.button1.Label = "Сформировать файл для TopVisor";
            this.button1.Name = "button1";
            this.button1.OfficeImageId = "FilePrintPreview";
            this.button1.ShowImage = true;
            this.button1.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button1_Click);
            // 
            // Button_KC_CreateMenu
            // 
            this.Button_KC_CreateMenu.Label = "Сформировать меню";
            this.Button_KC_CreateMenu.Name = "Button_KC_CreateMenu";
            this.Button_KC_CreateMenu.OfficeImageId = "FilePrintPreview";
            this.Button_KC_CreateMenu.ShowImage = true;
            this.Button_KC_CreateMenu.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Button_KC_CreateMenu_Click);
            // 
            // group_misc
            // 
            this.group_misc.Items.Add(this.WordsHighligherRibbonButton);
            this.group_misc.Items.Add(this.RemoveColumnsWithoutData_RibbonButton);
            this.group_misc.Label = "Разное";
            this.group_misc.Name = "group_misc";
            // 
            // WordsHighligherRibbonButton
            // 
            this.WordsHighligherRibbonButton.Label = "Выделение ячеек в диапазоне";
            this.WordsHighligherRibbonButton.Name = "WordsHighligherRibbonButton";
            this.WordsHighligherRibbonButton.OfficeImageId = "FormatPainter";
            this.WordsHighligherRibbonButton.ShowImage = true;
            this.WordsHighligherRibbonButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.WordsHighligherRibbonButton_Click);
            // 
            // AboutGroup
            // 
            this.AboutGroup.Items.Add(this.AboutButton);
            this.AboutGroup.Label = "О программе";
            this.AboutGroup.Name = "AboutGroup";
            // 
            // AboutButton
            // 
            this.AboutButton.Label = "О программе";
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.OfficeImageId = "Info";
            this.AboutButton.ShowImage = true;
            this.AboutButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AboutButton_Click);
            // 
            // RemoveColumnsWithoutData_RibbonButton
            // 
            this.RemoveColumnsWithoutData_RibbonButton.Label = "Удалить столбцы без данных";
            this.RemoveColumnsWithoutData_RibbonButton.Name = "RemoveColumnsWithoutData_RibbonButton";
            this.RemoveColumnsWithoutData_RibbonButton.OfficeImageId = "FileDropSqlDatabase";
            this.RemoveColumnsWithoutData_RibbonButton.ShowImage = true;
            this.RemoveColumnsWithoutData_RibbonButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.RemoveColumnsWithoutData_RibbonButton_Click);
            // 
            // webtronicsRibbon
            // 
            this.Name = "webtronicsRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.mainTabWebtronics);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.mainTabWebtronics.ResumeLayout(false);
            this.mainTabWebtronics.PerformLayout();
            this.mainRibbonTools.ResumeLayout(false);
            this.mainRibbonTools.PerformLayout();
            this.KC_TOOLS.ResumeLayout(false);
            this.KC_TOOLS.PerformLayout();
            this.group_misc.ResumeLayout(false);
            this.group_misc.PerformLayout();
            this.AboutGroup.ResumeLayout(false);
            this.AboutGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab mainTabWebtronics;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup mainRibbonTools;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton contentsCreateRibbon;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup KC_TOOLS;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton KeyCollectorRibbonSummFrequency;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton UrlToHyperlinkRibbonButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton filterToNewSheetRibbonButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BooksMergeRibbonButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton TransormSheetForGoogleSheets;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SortColumnsOnAllSheetsButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AboutButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup AboutGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group_misc;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton WordsHighligherRibbonButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton Button_KC_CreateMenu;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ColumnsWidthChanger_RibbonButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton RemoveColumnsWithoutData_RibbonButton;
    }

    partial class ThisRibbonCollection
    {
        internal webtronicsRibbon Ribbon1
        {
            get { return this.GetRibbon<webtronicsRibbon>(); }
        }
    }
}
